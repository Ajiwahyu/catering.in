<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PaymentController extends Controller
{
    public function index(){
        $active = 'Payment';
        $payments = Payment::orderBy('created_at', 'DESC')->paginate(5);

        return view('payments.index', compact('payments', 'active'));
    }

    public function edit(Request $req, $id){
        
        $active = 'Payment';
        $payment = Payment::find($id);
        $total = collect($payment->order->details)->sum(function($q){
            return $q->price * $q->qty;
        });

        return view('payments.confirm', compact('active', 'payment', 'total'));
    }
}