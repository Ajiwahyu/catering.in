<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $active = 'Category';
        $category = Category::orderBy('created_at', 'DESC')->paginate(10);

        return view('categories.index', compact('category', 'active'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    //  insert item category to database.
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:50|unique:categories',
        ]);
        
        $request->request->add(['slug' => $request->name]);
        
        Category::create([
            'name' => $request->name,
            'slug' => $request->slug,
        ]);

        return redirect(route('category.index'))->with('success', 'Kategori baru ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);
        $active = 'Category';
        return view('categories.edit', compact('category', 'active'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string|max:50|unique:categories,name,' . $id,
            'status' => 'required',
        ]);

        $category = Category::find($id);

        $category->update([
            'name' => $request->name,
            'status' => $request->status,
        ]);

        return redirect(route('category.index'))->with('success', 'Kategory berhasil dirubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $category = Category::find($id);

        $category = Category::withCount(['product'])->find($id);

        if($category->product_count == 0){
            $category->delete();
            return redirect(route('category.index'))->with('success', 'Kategori berhasil dihapus');
        }

        return redirect(route('category.index'))->with('error', 'Kategori sedang digunakan');
    }
}