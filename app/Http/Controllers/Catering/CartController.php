<?php

namespace App\Http\Controllers\Catering;

use App\Http\Controllers\Controller;
use App\Mail\CustomerRegisterMail;
use Illuminate\Http\Request;
use App\Models\Product;
use Illuminate\Support\Facades\DB;
use App\Models\Customer;
use App\Models\Order;
use Illuminate\Support\Str;
use App\Models\OrderDetail;
use App\Models\Payment;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class CartController extends Controller
{
    public function addToCart(Request $request){
        $this->validate($request, [
            'product_id' => 'required|exists:products,id', //PASTIKAN PRODUCT_IDNYA ADA DI DB
            'qty' => 'required|integer|min:10' //PASTIKAN QTY YANG DIKIRIM INTEGER
        ]);

        $carts = $this->getCarts();

        //CEK JIKA CARTS TIDAK NULL DAN PRODUCT_ID ADA DIDALAM ARRAY CARTS
        if ($carts && array_key_exists($request->product_id, $carts)) {
            //MAKA UPDATE QTY-NYA BERDASARKAN PRODUCT_ID YANG DIJADIKAN KEY ARRAY
            $carts[$request->product_id]['qty'] += $request->qty;
        } else {
            //SELAIN ITU, BUAT QUERY UNTUK MENGAMBIL PRODUK BERDASARKAN PRODUCT_ID
            $product = Product::find($request->product_id);
            //TAMBAHKAN DATA BARU DENGAN MENJADIKAN PRODUCT_ID SEBAGAI KEY DARI ARRAY CARTS
            $carts[$request->product_id] = [
                'qty' => $request->qty,
                'product_id' => $product->id,
                'product_name' => $product->name,
                'product_price' => $product->price,
                'product_image' => $product->image
            ];
        }
        
        //JANGAN LUPA UNTUK DI-ENCODE KEMBALI, DAN LIMITNYA 2800 MENIT ATAU 48 JAM
        $cookie = cookie('catering-in', json_encode($carts), 2880);
        //STORE KE BROWSER UNTUK DISIMPAN
        return redirect()->back()->cookie($cookie)->with('info', 'Product berhasil ditambahkan ke cart');

    }

    public function listCart()
    {
        $id = Auth::guard('customer')->id();
        
        $customer = Customer::where('id', $id)->first();
        //MENGAMBIL DATA DARI COOKIE
        $carts = json_decode(request()->cookie('catering-in'), true);
        //UBAH ARRAY MENJADI COLLECTION, KEMUDIAN GUNAKAN METHOD SUM UNTUK MENGHITUNG SUBTOTAL
        $subtotal = collect($carts)->sum(function($q) {
            return $q['qty'] * $q['product_price']; //SUBTOTAL TERDIRI DARI QTY * PRICE
        });
        //LOAD VIEW CART.BLADE.PHP DAN PASSING DATA CARTS DAN SUBTOTAL
        return view('catering.cart', compact('carts', 'subtotal', 'customer'));
    }

    public function updateCart(Request $request)
    {   
        //AMBIL DATA DARI COOKIE
        $carts = $this->getCarts();
        //KEMUDIAN LOOPING DATA PRODUCT_ID, KARENA NAMENYA ARRAY PADA VIEW SEBELUMNYA
        //MAKA DATA YANG DITERIMA ADALAH ARRAY SEHINGGA BISA DI-LOOPING

        foreach ($request->product_id as $key => $row) {
            //DI CHECK, JIKA QTY DENGAN KEY YANG SAMA DENGAN PRODUCT_ID = 0
            if ($request->qty[$key] == 0) {
                //MAKA DATA TERSEBUT DIHAPUS DARI ARRAY
                unset($carts[$row]);
            } else {
                //SELAIN ITU MAKA AKAN DIPERBAHARUI
                $carts[$row]['qty'] = $request->qty[$key];
            }
        }
        //SET KEMBALI COOKIE-NYA SEPERTI SEBELUMNYA
        $cookie = cookie('catering-in', json_encode($carts), 2880);
        //DAN STORE KE BROWSER.
        return redirect()->back()->cookie($cookie);
    }

    public function deleteCart($id){
        $carts = $this->getCarts();

        unset($carts[$id]);

        $cookie = cookie('catering-in', json_encode($carts), 2880);

        return redirect()->back()->cookie($cookie);
        
    }

    private function getCarts()
    {
        $carts = json_decode(request()->cookie('catering-in'), true);
        $carts = $carts != '' ? $carts:[];
        return $carts;
    }

    public function checkout()
    {
        $carts = $this->getCarts(); //MENGAMBIL DATA CART
        //MENGHITUNG SUBTOTAL DARI KERANJANG BELANJA (CART)
        $subtotal = collect($carts)->sum(function($q) {
            return $q['qty'] * $q['product_price'];
        });
        //ME-LOAD VIEW CHECKOUT.BLADE.PHP DAN PASSING DATA PROVINCES, CARTS DAN SUBTOTAL
        return view('catering.checkout', compact('carts', 'subtotal'));
    }

    public function processCheckout(Request $request)
    {
        //INISIASI DATABASE TRANSACTION
        //DATABASE TRANSACTION BERFUNGSI UNTUK MEMASTIKAN SEMUA PROSES SUKSES UNTUK KEMUDIAN DI COMMIT AGAR DATA BENAR BENAR DISIMPAN, JIKA TERJADI ERROR MAKA KITA ROLLBACK AGAR DATANYA SELARAS
        DB::beginTransaction();
        try {
            //CHECK DATA CUSTOMER BERDASARKAN EMAIL
            // $customer = Customer::where('email', $request->email)->first();
            
            //JIKA DIA TIDAK LOGIN DAN DATA CUSTOMERNYA ADA
            if (!Auth::guard('customer')->check()) {
                //MAKA REDIRECT DAN TAMPILKAN INSTRUKSI UNTUK LOGIN 
                return redirect(route('customer.login'))->with('info', 'Silahkan Login Terlebih Dahulu');
            } 
        
            //AMBIL DATA KERANJANG
            $carts = $this->getCarts();
            //HITUNG SUBTOTAL BELANJAAN
            $subtotal = collect($carts)->sum(function($q) {
                return $q['qty'] * $q['product_price'];
            });

            $customer = Customer::where('id', Auth::guard('customer')->user()->id)->first();
            
            //SIMPAN DATA ORDER
            $order = Order::create([
                'invoice' => Str::random(4) . '-' . time(), //INVOICENYA KITA BUAT DARI STRING RANDOM DAN WAKTU
                'customer_id' => $customer->id,
                'customer_name' => $customer->name,
                'customer_phone' => $customer->phone_number,
                'customer_address' => $customer->address,
                'subtotal' => $subtotal,
                'status' => false,
            ]);

            //LOOPING DATA DI CARTS
            foreach ($carts as $row) {
                //AMBIL DATA PRODUK BERDASARKAN PRODUCT_ID
                $product = Product::find($row['product_id']);
                //SIMPAN DETAIL ORDER
                OrderDetail::create([
                    'order_id' => $order->id,
                    'product_id' => $row['product_id'],
                    'price' => $row['product_price'],
                    'qty' => $row['qty'],
                ]);
            }
            
            //TIDAK TERJADI ERROR, MAKA COMMIT DATANYA UNTUK MENINFORMASIKAN BAHWA DATA SUDAH FIX UNTUK DISIMPAN
            DB::commit();

            $carts = [];
            //KOSONGKAN DATA KERANJANG DI COOKIE
            $cookie = cookie('catering-in', json_encode($carts), 2880);
            // Mail::to($request->email)->send(new CustomerRegisterMail($customer, $password));
            //REDIRECT KE HALAMAN FINISH TRANSAKSI
            return redirect(route('front.finish_checkout', $order->invoice))->cookie($cookie);
        } catch (\Exception $e) {
            //JIKA TERJADI ERROR, MAKA ROLLBACK DATANYA
            DB::rollback();
            //DAN KEMBALI KE FORM TRANSAKSI SERTA MENAMPILKAN ERROR
            return redirect()->back()->with(['error' => $e->getMessage()]);
        }
    }

    public function checkoutFinish($invoice)
    {   
        $customer_id = Auth::guard('customer')->user()->id;
        //AMBIL DATA PESANAN BERDASARKAN INVOICE
        $order = Order::with(['details'])->where('invoice', $invoice)->first();

        if($customer_id != $order->customer_id){
            return redirect(route('customer.order')); 
        }

        // model select raw 
        // $orderdetail = DB::table('order_details')
        //                 ->join('products', 'order_details.product_id', '=', 'products.id')
        //                 ->select('products.name', 'order_details.*')
        //                 ->where('order_details.order_id', '=', $order->id)
        //                 ->get();

        // eloquent relationship

        $total = collect($order->details)->sum(function($q){
            return $q->price * $q->qty;
        });

        $orderdetail = $order->details;

        $payment = Payment::where('order_id', $order->id)->first();

        //LOAD VIEW checkout_finish.blade.php DAN PASSING DATA ORDER
        return view('catering.checkout_finish', compact('order', 'orderdetail', 'total', 'payment'));
    }
}