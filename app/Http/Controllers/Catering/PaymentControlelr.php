<?php

namespace App\Http\Controllers\Catering;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class PaymentControlelr extends Controller
{
    public function paymentProses(Request $request, $id){
        // dd($id);
        $this->validate($request, [
            'order_id' => $id,
            'name_transfer' => 'required|max:225|string',
            'name_bank_transfer' => 'required|string',
            'amount' => 'required|numeric',
            'transfer_date' => 'required|',
            'image_transfer' => 'required|image|mimes:jpeg,png,jpg|max:2048',
        ]);
        

        DB::beginTransaction();
        try {
            // $order = Order::find($id);

            $imageName = $this->uploadImage($request->image_transfer, null);
            
            Payment::create([
                'order_id' => $id,
                'name_transfer' => $request->name_transfer,
                'name_bank_transfer' => $request->name_bank_transfer,
                'transfer_date' => $request->transfer_date,
                'amount' => $request->amount,
                'image_transfer' => $imageName,
                'status' => false,
            ]);

            DB::commit();

            return redirect()->back()->with('success', 'Konfirmasi pembayaran sedang direview');

        } catch (\Exception $th) {
            DB::rollBack();

            return redirect()->back()->with(['error' => $th->getMessage()]);
        }
    }

    public function paymentConfirm(Request $request){
        $payment = Payment::find($request->payment_id);
        $payment->order->status = true;
        $payment->status = true;
        $payment->order->save();
        $payment->save();

        return redirect()->back()->with('success', 'Pembayaran barhasil masuk');
    }

    public function uploadImage($imgProduct, $imageDB){
        $imageName = $imageDB;
        if(!empty($imgProduct)){
            $imageName = md5(time() . '.' . $imgProduct->extension());

            Storage::putFileAs(
                'public/images/payment',
                $imgProduct,
                $imageName,
            );

            if ($imageDB != null) {
                Storage::delete('/public/images/product/'.$imageDB);
            }
        }
        return $imageName;
    }
}