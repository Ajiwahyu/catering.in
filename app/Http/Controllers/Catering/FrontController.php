<?php

namespace App\Http\Controllers\Catering;

use App\Http\Controllers\Controller;
use App\Mail\ContactMail;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\Customer;
use Illuminate\Support\Facades\Mail;

class FrontController extends Controller
{
    public function index(){
        $active = 'Home';
        $products = Product::orderBy('created_at', 'DESC')->paginate(2);

        return view('catering.index', compact('products', 'active'));
    }

    public function product(){
        $products = Product::orderBy('created_at', 'DESC')->paginate(8);
        $active = 'Menu';

        return view('catering.product', compact('products', 'active'));
    }

    public function show($slug){
        $product = Product::with(['category'])->where('slug', $slug)->first();
        $products = Product::inRandomOrder()->limit(8)->get();
        $active = 'Oke';

        return view('catering.detail_product', compact('product', 'products', 'active'));
    }

    public function getFaq(){
        $active = 'FAQ';
        return view('catering.faq', compact('active'));
    }

    public function getContact(){
        $active = 'Contact';
        return view('catering.contact', compact('active'));
    }

    public function sendEmail(Request $request){
        $this->validate($request, [
            'firstName' => 'required',
            'lastName' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'message' => 'required',
        ]);

        Mail::to($request->email)->send(new ContactMail);
        return redirect()->back()->with('succes', 'Your Email has been sent');
    }
    
    // public function verifyCustomerRegistration($token)
    // {
    //     //JADI KITA BUAT QUERY UNTUK MENGMABIL DATA USER BERDASARKAN TOKEN YANG DITERIMA
    //     $customer = Customer::where('activate_token', $token)->first();
    //     if ($customer) {
    //         //JIKA ADA MAKA DATANYA DIUPDATE DENGNA MENGOSONGKAN TOKENNYA DAN STATUSNYA JADI AKTIF
    //         $customer->update([
    //             'activate_token' => '',
    //             'status' => 1
    //         ]);
    //         //REDIRECT KE HALAMAN LOGIN DENGAN MENGIRIMKAN FLASH SESSION SUCCESS
    //         return redirect(route('customer.login'))->with(['success' => 'Verifikasi Berhasil, Silahkan Login']);
    //     }
    //     //JIKA TIDAK ADA, MAKA REDIRECT KE HALAMAN LOGIN
    //     //DENGAN MENGIRIMKAN FLASH SESSION ERROR
    //     return redirect(route('customer.login'))->with(['error' => 'Invalid Verifikasi Token']);
    // }

    
}