<?php

namespace App\Http\Controllers\Catering;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Customer;
use App\Models\Order;

class ProfileController extends Controller
{
   
    public function myProfile()
    {
        $active = 'p';
        $customer = Customer::where('id', Auth::guard('customer')->user()->id)->first();

        return view('catering.my_profile', compact('active', 'customer'));
    }

    public function updateMyProfile(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|max:225',
            'email' => 'required|email|unique:customers,email,'.Auth::guard('customer')->user()->id,
            'phone_number' => 'required',
            'address' => 'required|max:500',
        ]);

        $customer = Customer::find($id);
        $customer->name = $request->name;
        $customer->email = $request->email;
        $customer->phone_number = $request->phone_number;
        $customer->address = $request->address;

        $customer->save();

        return redirect()->back()->with('success', 'Profil berhasil diupdate');
    }

    public function myOrder(Request $request){
        $active = 'p';
        $order = Order::where('customer_id', Auth::guard('customer')->user()->id)->get();
        if($request->q != ''){
            if($request->q == 0){
                $order = $order->where('status', $request->q);
            } else if($request->q == 1) {
                $order = $order->where('status', $request->q);
            }
        }

        return view('catering.my_order', compact('active', 'order'));
    }

}