<?php

namespace App\Http\Controllers\Catering;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function registerForm()
    {
        return view('catering.register');
    }

    public function register(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|unique:customers,email',
            'phone' => 'required|max:15',
            'address' => 'required|max:255',
            'password' => 'required',
        ]);

        Customer::create([
            'name' => $request->name,
            'email' => $request->email,
            'phone_number' => $request->phone,
            'address' => $request->address,
            'activate_token' => '',
            'status' => true,
            'password' => $request->password,
        ]);

        return redirect(route('customer.login'));
    }
}