<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product = Product::with(['category'])->orderBy('created_at', 'DESC');
        if(request()->q != ''){
            $product = $product->where('name', 'LIKE', '%' . request()->q . '%');
        }
        $product = $product->paginate(5);
        $active = "Product";

        return view('products.index', compact('product', 'active'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::where('status', 1)->orderBy('name', 'DESC')->get();
        $active = "Product";

        return view('products.create', compact('category', 'active'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //VALIDASI REQUESTNYA
        $this->validate($request, [
            'name' => 'required|string|max:100',
            'description' => 'required',
            'category_id' => 'required|exists:categories,id', //CATEGORY_ID KITA CEK HARUS ADA DI TABLE CATEGORIES DENGAN FIELD ID
            'price' => 'required|integer',
            'image' => 'required | image | mimes:png,jpeg,jpg |max:2048', //GAMBAR DIVALIDASI HARUS BERTIPE PNG,JPG DAN JPEG
        ]);

        // upload image to storage
        $imageName = $this->uploadImage($request->image, null);

        //SETELAH FILE TERSEBUT DISIMPAN, KITA SIMPAN INFORMASI PRODUKNYA KEDALAM DATABASE
        Product::create([
            'name' => $request->name,
            'slug' => $request->name,
            'category_id' => $request->category_id,
            'description' => $request->description,
            'image' => $imageName, //PASTIKAN MENGGUNAKAN VARIABLE FILENAM YANG HANYA BERISI NAMA FILE SAJA (STRING)
            'price' => $request->price,
            'status' => $request->status,
        ]);
        
        
        return redirect(route('product.index'))->with(['success' => 'Produk Baru Ditambahkan']);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        $category = Category::where('status', 1)->orderBy('name', 'DESC')->get();
        $active = "Product";
        return view('products.edit', compact('active', 'product', 'category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string|max:100',
            'description' => 'required',
            'category_id' => 'required|exists:categories,id', //CATEGORY_ID KITA CEK HARUS ADA DI TABLE CATEGORIES DENGAN FIELD ID
            'price' => 'required|integer',
            'image' => 'nullable | image | mimes:png,jpeg,jpg |max:2048', //GAMBAR DIVALIDASI HARUS BERTIPE PNG,JPG DAN JPEG
        ]);

        $product = Product::find($id);
        
        $imageName = $this->uploadImage($request->image, $product->image);
        // dd($imageName);
        
        $product->update([
            'name' => $request->name,
            'description' => $request->description,
            'status' => $request->status,
            'category_id' => $request->category_id,
            'price' => $request->price,
            'image' => $imageName,
        ]);

        return redirect(route('product.index'))->with('success', 'Product berhasil diedit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);

        Storage::delete('/public/images/product/'.$product->image);

        $product->delete();

        return redirect(route('product.index'))->with('success', 'Product berhasil dihapus');
    }

    public function uploadImage($imgProduct, $imageDB){
        $imageName = $imageDB;
        if(!empty($imgProduct)){
            $imageName = md5(time() . '.' . $imgProduct->extension());

            Storage::putFileAs(
                'public/images/product',
                $imgProduct,
                $imageName,
            );

            if ($imageDB != null) {
                Storage::delete('/public/images/product/'.$imageDB);
            }
        }
        return $imageName;
    }
}