<?php

namespace App\View\Components;

use App\Models\Product;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Illuminate\View\Component;

class Menu extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $active;
    public function __construct($active)
    {
        $this->active = $active;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        $user = auth()->id();

        return view('components.menu', ['active' => $this->active, 'user' => $user]);
    }

    public function list(){
        return [
            [
                'label' => 'Category',
                'icon' => 'fas fa-filter',
                'link' => 'category.index',
            ],
            [
                'label' => 'Product',
                'icon' => 'fas fa-utensils',
                'link' => 'product.index',
            ],
            [
                'label' => 'Customer',
                'icon' => 'fas fa-users',
                'link' => 'customer.index',
            ],
            [
                'label' => 'Order',
                'icon' => 'fas fa-tasks',
                'link' => 'order.index',
            ],
            [
                'label' => 'Payment',
                'icon' => 'fas fa-money-bill-wave',
                'link' => 'payment.index',
            ],
            // [
            //     'label' => 'Sign out',
            //     'icon' => 'fas fa-sign-out-alt',
            //     'link' => 'logout',
            // ],
        ];
    }

    public function isActive($label){
        return $label == $this->active;
    }

    public function getUser($id){
        return User::find($id);
    }

}