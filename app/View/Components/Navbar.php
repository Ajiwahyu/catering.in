<?php

namespace App\View\Components;

use Illuminate\View\Component;
use Illuminate\Support\Facades\Cookie;

class Navbar extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    protected $active;
    public function __construct($active)
    {
        $this->active = $active;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        $cookie = json_decode(cookie::get('catering-in'), true);
        return view('components.navbar', compact('cookie'));
    }

    public function lists(){
        return [
            [
                'label' => 'Home',
                'link' => 'front.index',
            ],
            [
                'label' => 'Menu',
                'link' => 'front.product',
            ],
            [
                'label' => 'FAQ',
                'link' => 'front.faq',
            ],
            [
                'label' => 'Contact',
                'link' => 'front.contact',
            ],
        ];
    }

    public function isActive($label){
        return $label == $this->active;
    }
}