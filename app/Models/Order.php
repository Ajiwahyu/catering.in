<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function getStatusPayment($v){
        if($v === 0){
            return "<span class='badge badge-info'>Belum dibayar</span>";
        } else {
            return "<span class='badge badge-success'>Dibayar</span>";
        }
    }

    // public function product(){
    //     return $this->belongsToMany(Product::class, 'order_details');
    // }

    public function details(){
        return $this->hasMany(OrderDetail::class);
    }

}