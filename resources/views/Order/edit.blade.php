@extends('layouts.admin')

@section('title')
<title>Admin | Orders</title>
@endsection

@section('content')
<main class="main">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Dashboard</li>
        <li class="breadcrumb-item active">{{$active}}</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header bg-dark">
                            <h4 class="card-title">Invoice</h4>
                        </div>
                        <div class="card-body p-3">
                            <div class="row">
                                <div class="col">
                                    <ul class="list-inline">
                                        <li>Nama Pemesan</li>
                                        <li>
                                            <h5>{{$orders->customer_name}}</h5>
                                        </li>
                                        <li>No. Telp</li>
                                        <li>
                                            <h5>{{$orders->customer_phone}}</h5>
                                        </li>
                                        <li>Alamat Pemesan</li>
                                        <li>
                                            <h5>{{$orders->customer_address}}</h5>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col">
                                    <ul class="list-inline">
                                        <li>Inovice</li>
                                        <li>
                                            <h5>{{$orders->invoice}}</h5>
                                        </li>
                                        <li>Tanggal Order</li>
                                        <li>
                                            <h5>{{$orders->created_at->format('d-m-Y')}}</h5>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col">
                                    <ul class="list-inline">
                                        <li>Status</li>
                                        <li>
                                            @if ($orders->status == 0)
                                            <h1 class="text-danger text-bold">Belum DiBayar</h1>
                                            @else
                                            <h1 class="text-success text-bold">Dibayar</h1>
                                            @endif
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="row p-2">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th class="text-right">Price</th>
                                            <th class="text-right">Qty</th>
                                            <th class="text-right">Subtotal</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($orders->details as $order)
                                        <tr>
                                            <td>{{$order->product->name}}</td>
                                            <td class="text-right">IDR. {{number_format($order->price)}}</td>
                                            <td class="text-right">{{$order->qty}}</td>
                                            <td class="text-right text-bold">IDR.
                                                {{ number_format($order->price * $order->qty)}}</td>
                                        </tr>
                                        @endforeach
                                        <tr>
                                            <td colspan="3" class="text-right text-bold">Total</td>
                                            <td class="text-right text-bold">IDR. {{number_format($total)}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection