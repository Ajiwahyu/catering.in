@extends('layouts.admin')

@section('title')
<title>Admin | Orders</title>
@endsection

@section('content')
<main class="main">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Dashboard</li>
        <li class="breadcrumb-item active">{{$active}}</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header bg-dark">
                            <h4 class="card-title">List Order</h4>
                        </div>
                        <div class="card-body p-0">
                            <table class="table table-striped table-dark">
                                <thead>
                                    <tr>
                                        <th scope="col">Invoice</th>
                                        <th scope="col">Customer Name</th>
                                        <th scope="col">Total</th>
                                        <th scope="col">Order Date</th>
                                        <th scope="col">Status</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($order as $item)
                                    <tr>
                                        <th>{{$item->invoice}}</th>
                                        <td>{{$item->customer_name}}</td>
                                        <td>IDR. {{number_format($item->subtotal)}}</td>
                                        <td>{{$item->created_at->format('d-m-Y')}}</td>
                                        <td>
                                            @if ($item->status === 0)
                                            <span class='badge badge-danger'>Belum dibayar</span>
                                            @else
                                            <span class='badge badge-success'>Dibayar</span>
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{route('order.edit', $item->id)}}"
                                                class="btn btn-warning btn-sm">Edit</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection