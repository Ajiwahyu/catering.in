@extends('layouts.catering2')

@section('head')
<title>FAq</title>
<link rel="stylesheet" href="{{asset('assets/css/standart.css')}}">
@endsection

@section('content')
<div class="container py-5 .content__container">
    <div class="row pt-5">
        <div class="col text-center header">
            <h1 class="">FAQ</h1>
            <p>Hal yang sering ditanyakan</p>
        </div>
    </div>
    <div class="row justify-content-center py-5">
        <div class="col-md-6">
            <div class="accordion accordion-flush" id="accordionFlushExample">
                <div class="accordion-item">
                    <h2 class="accordion-header" id="flush-headingOne">
                        <button class="accordion-button collapsed button__faq" id="buke" type="button"
                            data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false"
                            aria-controls="flush-collapseOne">
                            Apa itu Catering.in
                        </button>
                    </h2>
                    <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne"
                        data-bs-parent="#accordionFlushExample">
                        <div class="accordion-body">
                            Catering.in Adalah layanan pemesanan makanan & minuman dalam jumalah besar maupun kecil baik
                            individu atau perusahaan.
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="flush-headingTwo">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
                            Apakah saya bisa memilih menu tertentu yang tidak ada pada menu ?
                        </button>
                    </h2>
                    <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo"
                        data-bs-parent="#accordionFlushExample">
                        <div class="accordion-body">
                            Selain menu yang ditampilkan diwebsite kami, anda dapat membuat menu sendiri atau meminta
                            penawaran harga menu dengan menghubungi tim kami melalui <b>Contact</b>
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="flush-headingThree">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#flush-collapseThree" aria-expanded="false"
                            aria-controls="flush-collapseThree">
                            Apa itu minimum order ?
                        </button>
                    </h2>
                    <div id="flush-collapseThree" class="accordion-collapse collapse"
                        aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
                        <div class="accordion-body">
                            Setiap mitra catering.in dapat menentukan pemesanan minimum untuk pesanannya. Hal ini kami
                            lakukan untuk menjamin setiap produk tetap dalam harga terjangkau dan tanpa mark-up apapun.
                            Jika anda memerlukan pemesanan yang lebih kecil dari minimum order, silahkan hubungi tim
                            kami.
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="flush-headingFour">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#flush-collapseFour" aria-expanded="false"
                            aria-controls="flush-collapseFour">
                            Apakah catering.in dapat melayani pemesanan dalam jumlah besar ?
                        </button>
                    </h2>
                    <div id="flush-collapseFour" class="accordion-collapse collapse" aria-labelledby="flush-headingFour"
                        data-bs-parent="#accordionFlushExample">
                        <div class="accordion-body">
                            Ya tentu karena itu spesialisasi dari kami, anda cukup memesan mandiri melalui website, atau
                            anda dapat berkonsultasi dengan tim kami untuk mendapatkan penawaran menarik.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    $(document).ready(function(){
        $('.navbar').removeClass('change')  
  })
</script>
@endsection