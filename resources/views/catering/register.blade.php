<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
  <link rel="stylesheet" href="{{asset('assets/css/catering/register.css')}}">
  <link rel="stylesheet" href="{{asset('assets/css/standart.css')}}">
  <!-- Bootstrap CSS -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">

  <title>Register</title>
</head>

<body>
  <div class="container main__content d-flex align-items-center p-0">
    <div class="row d-flex bg p-5 w-100">
      <div class="col-md d-flex justify-content-center align-items-center">
        <img src="{{asset('assets/img/undraw_healthy_options_sdo3.png')}}" alt="logo-login">
      </div>
      <div class="col-md">
        <div class="title text-center">
          <h3>Save your account now</h3>
          <p class="m-0">with login you can start ordering</p>
          <p class="m-0">safely and comfortably</p>
        </div>
        <div class="row p-5 py-2">
          <div class="col px-4">
            <form action="{{route('customer.post_register')}}" method="POST">
              @csrf

              <div class="form-group input__in mt-3">
                <input type="text" name="name" class="form-control border-0 input__form" placeholder="Full Name"
                  value="{{old('name')}}">
              </div>
              @error('name')
              <small class="text-danger px-2">{{$message}}</small>
              @enderror
              <div class="form-group input__in mt-3">
                <input type="email" name="email" class="form-control border-0 input__form" value="{{old('email')}}"
                  placeholder="Email">
              </div>
              @error('email')
              <small class="text-danger px-2">{{$message}}</small>
              @enderror
              <div class="form-group input__in mt-3">
                <input type="text" name="phone" class="form-control border-0 input__form" value="{{old('phone')}}"
                  placeholder="Number Phone">
              </div>
              @error('phone')
              <small class="text-danger px-2">{{$message}}</small>
              @enderror
              <div class="form-group input__in mt-3">
                <textarea class="form-control border-0 input__form" name="address"
                  placeholder="Full Address">{{old('address')}}</textarea>
              </div>
              @error('address')
              <small class="text-danger px-2">{{$message}}</small>
              @enderror
              <div class="form-group my-3 input__in">
                <input type="password" class="form-control border-0 input__form" name="password" placeholder="Password">
              </div>
              <div class="mb-3 text-center py-2">
                <button type="submit" class="btn btn-success">Register</button>
              </div>

              <div class="login">
                <p class="p-0 m-0">Already have an account?</p>
                <a href="{{route('customer.login')}}" class="fw-bold text-danger text-decoration-none">Login</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Option 1: Bootstrap Bundle with Popper -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous">
  </script>
</body>

</html>