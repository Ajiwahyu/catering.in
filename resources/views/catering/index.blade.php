@extends('layouts.catering2')

@section('head')
<title>CateringIn - Best Food</title>
<link rel="stylesheet" href="{{asset('assets/css/catering/homepage.css')}}">
@endsection

@section('content')
{{-- <div class="container-fluid my-5">
  <div class="container py-5">
    <div class="row">
      <div class="col">
        @foreach ($products as $product)
        <div class="card" style="width: 18rem;">
          <img src="{{asset('storage/images/product/'.$product->image)}}" class="card-img-top"
alt="{{$product->slug}}">
<div class="card-body">
  <h5 class="card-title">{{$product->name}}</h5>
  <p>{{'Rp.'. number_format($product->price)}}</p>
  <a href="{{ url('/product/'.$product->slug) }}" class="btn btn-primary">Detail</a>
</div>
</div>
@endforeach
</div>
</div>
</div>
</div> --}}
{{-- Hero --}}
<div class="container-fluid" id="container-view-home">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="row p-5 px-0" id="landing-page">
          <div class="col-md-6 d-flex justify-content-center py-5 px-0">
            <img src="{{asset('assets/img/undraw_Hamburger_8ge6.svg')}}" alt="hero-image" width="600">
          </div>
          <div class="col-md-6 d-flex justify-content-start">
            <div class="container-fluid p-5 px-0">
              <h1>Fresh & Healty</h1>
              <h1>Delicious Food Is Waiting</h1>
              <h1>For You</h1>
              <p>Here there are lots of quality food and drinks that are processed directly by professional and
                experienced chefs and supervised by nutritionists</p>
              <div class="p-1 mt-5 bd-highlight d-flex justify-content-center">
                <button type="button" class="btn btn-success" onclick="location.href='{{route('front.product')}}'">View
                  Food
                  Menu</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

{{-- section popular menu --}}
<div class="container-fluid" id="container-menu">
  <div class="container">
    <div class="row">
      <div class="col-md-4 p-0 px-5 d-flex align-items-center">
        <div>
          <h2 class="fw-bold">Most Popular Menu</h2>
          <p>A meal that is frequently ordered this month. The popular menu can change any time depending on the trend
            season</p>
          <div class="d-flex justify-content-center p-2 mt-5">
            <button type="button" class="btn btn-outline-success" onclick="location.href='{{route('front.product')}}'"
              name="button">View Menu</button>
          </div>

        </div>
      </div>
      <div class="col-md-8 d-flex flex-row justify-content-between px-5">

        @foreach ($products as $product)
        <div class="content p-2 shadow content__popular" id="menu-content">
          <div class="">
            <img src="{{asset('storage/images/product/'.$product->image)}}" class="img__popular"
              alt="{{$product->slug}}">
          </div>
          <div class="info__popular px-2">
            <h4 class="text-center fw-bold my-3 title__popular">{{Str::limit($product->name, 15)}}</h4>
            <p class="text-center text-dark">{{Str::limit($product->description, 100)}}</p>
            <div class="d-flex mt-2 mb-5">
              <div class="p-1 bd-highlight mr-4">
                <button type="button" class="btn btn-outline-warning btn-sm">min 10 pax</button>
              </div>
              <div class="p-1 bd-highlight ">
                <button type="button" class="btn btn-outline-warning btn-sm">min pemesanan H-1</button>
              </div>
            </div>
          </div>

          <div class="d-flex px-2 justify-content-between">
            <div class="p-1">
              <a href="{{ url('/product/'.$product->slug) }}" class="btn btn-success btn-sm px-5">Detail</a>
            </div>
            <div class="p-1 align-self-center">
              <span class="fw-bold fs-6">IDR. 60.000</span>
            </div>
          </div>
        </div>
        @endforeach

      </div>
    </div>
  </div>
</div>

{{-- <div class="container-fluid">
  <ul>
    <li class="d-flex justify-content-between">
      <span>Chapter 1</span>
      <span>Ours Ago</span>
    </li>
  </ul>
</div> --}}

{{-- section our service --}}
<div class="container-fluid py-5" id="container-view-home-our">
  <div class="container">
    <div class="row p-0">
      <div class="col-md-12 p-0">
        <div class="row p-2 p-0" id="landing-page">

          <div class="col-md-12 p-0 text-center my-2">
            <h1 class="fw-bold">Our Awesome Service</h1>
          </div>

          <div class="col-md-12">
            <div class="row d-flex justify-content-center">
              <div class="col-md-8">
                <p class="text-center"> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                  incididunt ut labore
                  et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                  aliquip exea commodo consequat.</p>
              </div>
            </div>
          </div>

          <div class="row my-4 px-5">

            <div class="col-md-4 d-flex justify-content-center">
              <div class="bg-white shadow p-3" id="content__service">
                <div class="d-flex justify-content-center p-3">
                  <img src="{{asset('')}}assets/img/diet.svg" class="" width="150px" alt="">
                </div>
                <h4 class="text-center fw-bold">Quality Food</h4>
                <p class="text-center">The materials we use are of the highest quality and go through a rigorous
                  selection process</p>
                <div class="text-center mt-5 mb-3">
                  <a href="" class="text-decoration-none text-success fw-bold">read more</a>
                </div>
              </div>
            </div>

            <div class="col-md-4 d-flex justify-content-center">
              <div class="bg-white shadow p-3" id="content__service">
                <div class="d-flex justify-content-center p-3">
                  <img src="{{asset('assets/img/food.svg')}}" class="" width="150px" alt="">
                </div>
                <h4 class="text-center fw-bold">Quality Food</h4>
                <p class="text-center"> The materials we use are of the highest quality and go through a rigorous
                  selection process</p>
                <div class="text-center mt-5 mb-3">
                  <a href="" class="text-decoration-none text-success fw-bold">read more</a>
                </div>
              </div>
            </div>

            <div class="col-md-4 d-flex justify-content-center">
              <div class="bg-white shadow p-3" id="content__service">
                <div class="d-flex justify-content-center p-3">
                  <img src="{{asset('assets/img/delivery-truck.svg')}}" class="" width="150px" alt="">
                </div>
                <h4 class="text-center fw-bold">Quality Food</h4>
                <p class="text-center"> The materials we use are of the highest quality and go through a rigorous
                  selection process</p>
                <div class="text-center mt-5 mb-3">
                  <a href="" class="text-decoration-none text-success fw-bold">read more</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


{{-- section abilities --}}
<div class="container-fluid my-5" id="container-view-home-down">
  <div class="container">

    <div class="row">
      <div class="col-md-12">
        <div class="row p-5" id="landing-page">
          <div class="col-md-6  d-flex justify-content-center p-5">
            <img src="{{asset('assets/img/undraw_online_groceries_a02y.svg')}}" alt="gambar-page1" width="500">
          </div>
          <div class="col-md-6 d-flex align-items-center">
            <div class="container-fluid p-3">
              <h1 class="fw-bold">Simple Way To Order <br> Your Food</h1>
              <p>One of the visions of our company is to prioritize convenience for consumers, be it the ease of
                choosing
                a menu or ease of payment. With 3 steps you can order the highest quality catering for your event. </p>
              <ul class="list-inline">
                <li class="my-4 fs-5 fw-bold">
                  <span class="badge me-3">01</span>Select Your Food
                </li>
                <li class="my-4 fs-5 fw-bold">
                  <span class="badge me-3">02</span>Add To Cart
                </li>
                <li class="my-4 fs-5 fw-bold">
                  <span class="badge me-3">03</span>Order Your Food
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@section('js')
<script>
  $(document).ready(function(){
    $(window).scroll(function(){
      if(this.scrollY > 40){
        $('.navbar').removeClass('change')
      } else {
        $('.navbar').addClass('change');
      }
    })
  })
</script>
@endsection