@extends('layouts.catering2')

@section('head')
<title>Menu Product</title>
<link rel="stylesheet" href="{{asset('assets/css/standart.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/menu-product1.css')}}">
{{-- <link rel="stylesheet" href="{{asset('assets/css/styleDetailMenu.css')}}"> --}}
@endsection

@section('content')
{{-- top Hero --}}
<div class="container-fluid" id="container-view-home">
  <div class="container">
    <div class="row">
      <div class="col-md-6 py-5">
        <img src="{{asset('assets/img/undraw_diet_ghvw.svg')}}" alt="gambar-page1" width="500">
      </div>
      <div class="col-md-6 d-flex justify-content-start">
        <div class="container-fluid p-5">
          <h1 class="hindai__font">Menu</h1>
          <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
            dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex
            ea commodo consequat.
            Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
            Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est
            laborum.
          </p>
        </div>
      </div>
    </div>
  </div>
</div>

{{-- testing --}}
<div class="container-fluid p-0 my-5" id="container-menu">
  <div class="container">
    <div class="row">
      <h1>All Menu</h1>
      @foreach ($products as $item)
      <div class="col-md-3 my-4">
        <div class="container-fluid d-flex justify-content-center">
          <div class="content">
            <a href="Pdetailsale4.html">
              <div class="content-overlay">
                <img class="content-image rounded" height="200" width="300"
                  src="{{asset('storage/images/product/'.$item->image)}}" />
              </div>
            </a>
          </div>
        </div>
        <h4 class="p-1 mt-3 mb-2 hindai__font">{{$item->name}}</h4>
        <div class="d-flex mb-3">
          <div class="p-1 bd-highlight mr-4">
            <button type="button" class="btn btn-outline-warning btn-sm">
              min 10 pax
            </button>
          </div>
          <div class="p-1 bd-highlight">
            <button type="button" class="btn btn-outline-warning btn-sm">
              min pemesanan H-1
            </button>
          </div>
        </div>
        <div class="d-flex bd-highlight detail">
          <div class="p-1 me-2 bd-highlight">
            <a class="btn-detail btn btn-success" href="{{route('front.show_product',$item->slug)}}">
              Detail
            </a>
          </div>
          <div class="p-1 flex-shrink-1 bd-highlight">
            <h6 class="mb-2 p-1 ms-5">
              <span>IDR {{number_format($item->price)}}</span>
            </h6>
          </div>
        </div>
      </div>
      @endforeach
    </div>


    <div class="row mt-5 mb-5">
      <div class="col-md-5">
        <hr>
      </div>
      <div class="col-md-2 d-flex justify-content-center align-middle">
        @if ($products->hasMorePages())
        <a href="{{$products->nextPageUrl()}}" class="btn btn-outline-success"><span
            class="align-middle fs-5 btn-more">More</span></a>
        @else
        <a href="{{$products->url(1)}}" class="btn btn-outline-success"><span
            class="align-middle fs-5 btn-more">First</span></a>
        @endif
        {{-- <button type="button" class="btn btn-outline-success">More</button> --}}
      </div>
      <div class="col-md-5">
        <hr>
      </div>
    </div>
  </div>

</div>
@endsection

@section('js')
<script>
  $(document).ready(function(){
    $(window).scroll(function(){
      if(this.scrollY > 40){
        $('.navbar').removeClass('change')
      } else {
        $('.navbar').addClass('change');
      }
    })
  })
</script>
@endsection