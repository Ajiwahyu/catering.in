@extends('layouts.catering2')

@section('head')
<title>Contact</title>
<link rel="stylesheet" href="{{asset('assets/css/standart.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/catering/profile.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/catering/contact.css')}}">
{{-- <link rel="stylesheet" href="{{asset('assets/css/styleDetailMenu.css')}}"> --}}
@endsection

@section('content')
<div class="container my-5 py-5">
    <div class="row justify-content-center">
        <div class="col-md-2 submenu p-3">
            <p class="fw-bold m-0">Our Office</p>
            <small class="d-block my-2">Jl.Super no 155 Dero Condong Catur Depok Sleman, DIY</small>
            <a href="https://www.google.com/maps/@-7.7505448,110.4102195,138m/data=!3m1!1e3"
                class="text-decoration-none fw-bold" target="_blank">See
                Maps</a>
            <p class="fw-bold my-2">Our Working Days</p>
            <small>Monday to Friday <br>08.00 - 20.00</small>
        </div>

        <div class="col-md-6 mainmenu">
            <div class="row">
                <form action="{{route('front.post_contact')}}" method="POST">
                    @csrf
                    <div class="row mb-3">
                        <div class="col-md-6">
                            <div class="col-md-12">
                                <label class="form-label">First Name</label>
                                <input type="text" name="firtName" class="form-control input__form" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-12">
                                <label class="form-label">Last Name</label>
                                <input type="text" name="lastName" class="form-control input__form" required>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-md-6">
                            <div class="col-md-12">
                                <label class="form-label">Email</label>
                                <input type="email" name="email" class="form-control input__form"
                                    placeholder="example@gmail.com" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-12">
                                <label class="form-label m-0">Phone</label>
                                <div class="input-group flex-nowrap mt-2">
                                    <span class="input-group-text">+62</span>
                                    <input type="number" name="phone" class="form-control input__form" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col">
                            <label class="form-label">message</label>
                            <textarea name="message" cols="20" rows="10" class="input__form form-control"
                                required></textarea>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col">
                            <button type="submit" class="btn btn__save w-100">Send</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>
@endsection

@section('js')
<script>
    $(document).ready(function(){
          $('.navbar').removeClass('change')
    })
</script>
@endsection