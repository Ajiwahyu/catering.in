<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Cart</title>

  <link rel="stylesheet" href="{{asset('assets/catering/css/bootstrap.css')}}">
</head>
<body>

  <div class="container py-5">
    <form action="{{route('front.update_cart')}}" method="post">
      @csrf
      <table>
        <table class="table table-dark">
          <thead>
            <tr>
              <th scope="col">Product</th>
              <th scope="col">Price</th>
              <th scope="col">Quantity</th>
              <th scope="col">Total</th>
            </tr>
          </thead>
          <tbody>
            @forelse ($carts as $cart)
                <tr>
                  <td>
                    <div class="image">
                      <img src="{{ asset('storage/images/product/'.$cart['product_image'])}}" width="200" alt="{{$cart['product_name']}}">
                      <span>{{$cart['product_name']}}</span>
                    </div>
                  </td>
                  <td>{{$cart['product_price']}}</td>
                  <td>
                    <input type="number" name="qty[]" value="{{ $cart['qty'] }}" class="input-text qty">
                    <input type="hidden" name="product_id[]" value="{{ $cart['product_id'] }}" class="form-control">
                  </td>
                  <td>Rp.{{ number_format($cart['product_price'] * $cart['qty'])}}</td>
                </tr>
            @empty
                <tr>
                  <td>tidak ada belanjaan</td>
                </tr>
            @endforelse
            @if (!empty($carts))
              <tr>
                <td class="">
                  <a href="{{route('front.index')}}">Lanjutkan Belanja</a>
                </td>
                <td class="" colspan="2">
                  <button class="btn btn-sm btn-primary">Update Cart</button>
                </td>
                <td class="">
                  <a href="{{route('front.checkout')}}">Checkout</a>
                </td>
              </tr>
            @endif
          </tbody>
        </table>
      </table>
    </form>

    <div class="sub_total d-flex justify-content-end p-3 bg-light">
      <span>Subtotal = Rp.{{ number_format($subtotal)}}</span>
    </div>
  </div>


  {{-- footer --}}
  <script src="{{asset('assets/catering/js/jquery-3.2.1.min.js')}}"></script>
  <script src="{{asset('assets/catering/js/bootstrap.min.js')}}"></script>
</body>
</html>