<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="{{asset('assets/css/cart.css')}}">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">

  <title>Checkout</title>
</head>

<body>
  <div class="container">
    <main>
      <!-- breadcrumb -->
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Checkout</li>
        </ol>
      </nav>
      <!-- list group -->
      <div class="row">
        @if (session('error'))
        {{session('error')}}
        @endif
        <div class="col-md-8 menu">
          <div class="mb-3 border-0 pt-3" style="max-width: 100%;">
            <form action="{{route('front.update_cart')}}" method="POST">
              @csrf
              @forelse ($carts as $cart)
              <div class="row mb-4">
                <div class="col-md-1" style="padding-left: 40px;">
                  <span>{{$loop->iteration}}.</span>
                </div>
                <div class="col-md-2">
                  <img src="{{asset('storage/images/product/'.$cart['product_image'])}}" class="img-fluid img-thumbnail"
                    alt="Sheep">
                </div>
                <div class="col-md-3">
                  <p>{{$cart['product_name']}}</p>
                </div>
                <div class="col-md-2 align-item-center">
                  <input type="number" name="qty[]" value="{{ $cart['qty'] }}" class="input-text input__qty" min="10">
                  <input type="hidden" name="product_id[]" value="{{ $cart['product_id'] }}" class="form-control">
                </div>
                <div class="col-md-3">
                  <p>IDR {{number_format($cart['product_price'] * $cart['qty'])}}</p>
                </div>
                <div class="col-md-1">
                  <a href="{{route('front.delete_cart', $cart['product_id'])}}"><img
                      src="{{asset('assets/img/Component 7 – 1.png')}}" alt="{{$cart['product_name']}}"></td></a>
                </div>
              </div>
              @empty
              <p>Tidak ada makanan yang dipilih</p>
              @endforelse
              @if ($carts)
              <div class="row mb-4">
                <div class="col d-flex justify-content-end px-4">
                  <button type="submit" class="btn btn-sm" style="background-color: #1DB954; color: white;">Update
                    Cart</button>
                </div>
              </div>
              @endif
            </form>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card mb-3 border-0" style="max-width: 100%; background-color: rgba(28, 185, 84, 0.1);">
            <div class="card-body"><b>Guest Checkout</b></div>
            <div class="card-body text-success">
              <span class="card-title"><img src="{{asset('')}}assets/img/Path 41.png" alt=""
                  style="padding-right: 10px;">{{$customer->email}}</span>
            </div>
          </div>
          <div class="card mb-3 border-0" style="max-width: 100%; background-color: rgba(28, 185, 84, 0.1);">
            <div class="card-body"><b>Shipping Information</b></div>
            <div class="card-body text-success">
              <span class="card-title"><img src="{{asset('')}}assets/img/edit-24px.png" alt=""
                  style="padding-right: 10px; padding-bottom: 10px;"> {{$customer->name}}</span>
              <br>
              <span class="card-title"><img src="{{asset('')}}assets/img/location_on-24px.png" alt=""
                  style="padding-right: 10px; padding-bottom: 10px;">{{$customer->address}}</span>
              <br>
              <span class="card-title"><img src="{{asset('')}}assets/img/phone-24px.png" alt=""
                  style="padding-right: 7px;">{{$customer->phone_number}}</span>
            </div>
          </div>
          <div class="card mb-3 border-0" style="max-width: 100%; background-color: rgba(28, 185, 84, 0.1);">
            <div class="card-body"><b>Payment Instruction</b></div>
            <div class="card-body text-success text-center">
              <img src="{{asset('assets/img/logo-bank-mandiri.png')}}" alt="" width="200px">
              <span class="mb-2 d-block">Saat ini hanya tersedia transfer ke bank mandiri</span>
            </div>
          </div>
          <div class="card mb-3 border-0 pt-3" style="max-width: 100%;">
            <div class="row">
              <div class="col-md-2">

              </div>
              <div class="col-md-5 text-end">
                <p>Subtotal :</p>
                <p>Shipping :</p>
                <p><b>Total Price :</b></p>
              </div>
              <div class="col-md-5 text-end">
                <p>IDR {{ number_format($subtotal)}}</p>
                <p>Free</p>
                <p><b>IDR {{ number_format($subtotal)}}</b></p>
              </div>
            </div>
            <hr>
          </div>
        </div>
      </div>
      <div class="card mb-3 border-0 col-12" style="max-width: 100%;">
        <div class="row">
          <div class="col-md-11">
            <a href="{{route('front.index')}}"><img src="{{asset('')}}assets/img/keyboard_backspace-24px.png" alt=""
                style="padding-left: 18px;"><span
                style="color: #F97400; padding-left: 10px; font-weight: bold;">Continue Shopping</span></a>
          </div>
          <div class="col-md-1">
            <form action="{{route('front.store_checkout')}}" method="post">
              @csrf
              <button type="submit" class="btn" style="background-color: #1DB954; color: white;">Checkout</button>
            </form>
          </div>
        </div>
      </div>
  </div>
  </div>
  </main>
  </div>

  <!-- Optional JavaScript; choose one of the two! -->

  <!-- Option 1: Bootstrap Bundle with Popper -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous">
  </script>
</body>

</html>