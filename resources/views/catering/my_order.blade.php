<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="{{asset('assets/css/catering/my_order.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/standart.css')}}">

    <link rel="stylesheet" href="{{asset('assets/plugins/fontawesome-free/css/all.css')}}">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <title>My Order</title>
    <script src="{{asset('assets/js/jquery.min.js')}}"></script>
</head>

<body>
    @include('layouts.catering.module.header')
    <div class="container pt-5 mt-5">
        @if (session('success'))
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="alert alert-success" role="alert">
                    {{session('success')}}
                </div>
            </div>
        </div>
        @endif
        <div class="row justify-content-center">
            <div class="col-md-2 submenu">
                <a href="{{route('customer.order')}}" class="submenu__list submenu__active">My Orders</a>
                <a href="{{route('customer.profile')}}" class="submenu__list">My Account</a>
                <a href="{{route('customer.logout')}}" class="submenu__list">Logout</a>
            </div>
            <div class="col-md-6 mainmenu px-2">
                <div class="row p-3">
                    <div class="col card mx-2 p-0 payment__status">
                        <a href="?q=0" class="card-body text-center text-white text-decoration-none">
                            <i class="far fa-money-bill-alt"></i><br>
                            Belum dibayar
                        </a>
                    </div>
                    <div class="col card mx-2 p-0 payment__status">
                        <a href="?q=1" class="card-body text-center text-white text-decoration-none">
                            <i class="far fa-money-bill-alt"></i><br>
                            Dibayar
                        </a>
                    </div>
                </div>

                {{-- list order --}}
                <table class="table table-bordered">
                    <thead class="bg-light">
                        <tr>
                            <th scope="col">Invoice</th>
                            <th scope="col" class="text-center">Status</th>
                            <th scope="col" class="text-center">Total</th>
                            <th scope="col" class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody class="table-order">

                        @forelse ($order as $item)
                        <tr>
                            <td>{{$item->invoice}}</td>
                            <td class="text-center">
                                @if ($item->status === 0)
                                <span class="badge bg-danger">Belum dibayar</span>
                                @else
                                <span class="badge bg-success">Dibayar</span>
                                @endif
                            </td>
                            <td class="text-center">IDR. {{number_format($item->subtotal)}}</td>
                            <td class="text-center">
                                <a href="{{route('front.finish_checkout', $item->invoice)}}"
                                    class="btn btn-warning btn-sm fw-bold">Lihat</a>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="4" class="text-center">
                                Tidak Ada Transaksi
                            </td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
                {{-- end list table --}}

            </div>
        </div>
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous">
    </script>
    <script>
        $(document).ready(function(){
        $('.navbar').removeClass('change')
          
      })
    </script>
</body>

</html>