<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Checkout</title>

  <link rel="stylesheet" href="{{asset('assets/catering/css/bootstrap.css')}}">
</head>

<body>

  <div class="container py-5">
    <h1>Informasi Checkout</h1>

    @if (session('error'))
    <div class="alert alert-danger">{{ session('error') }}</div>
    @endif
    <div class="row">
      <div class="error">
      </div>
      <div class="col-8 bg-secondary p-3">
        <h2>Informasi Pengiriman</h2>

        <div class="form bg-light p-2">
          <form action="{{route('front.store_checkout')}}" method="POST">
            @csrf
            <div class="form-group">
              <label for="nama">Nama Lengkap</label>
              <input type="text" class="form-control" name="name" id="nama">
              @error('name')
              <small class="text-danger">{{$message}}</small>
              @enderror
            </div>
            <div class="form-group row">
              <div class="col">
                <label>No. Telp</label>
                <input type="text" class="form-control" name="telp" placeholder="No.Telp">
                @error('telp')
                <small class="text-danger">{{$message}}</small>
                @enderror
              </div>
              <div class="col">
                <label>Email</label>
                <input type="email" class="form-control" name="email" placeholder="Email..">
                @error('email')
                <small class="text-danger">{{$message}}</small>
                @enderror
              </div>
            </div>
            <div class="form-group">
              <label>Address</label>
              @error('address')
              <small class="text-danger">{{$message}}</small>
              @enderror
              <textarea name="address" cols="30" rows="5" name="address" class="form-control"></textarea>
            </div>
        </div>
      </div>
      <div class="col-4 bg-light">
        <h2>Ringkasan Pesan</h2>
        <ul class="list-group">
          <li class="list-group-item d-flex justify-content-between align-items-center">
            <span>Product</span>
            <span>Total</span>
          </li>
          @foreach ($carts as $cart)
          <li class="list-group-item d-flex justify-content-between align-items-center">
            <div class="cot">
              {{ \Str::limit($cart['product_name'], 20) }}
              <span class="badge badge-primary badge-pill">{{ $cart['qty'] }}</span>
            </div>
            Rp.{{ number_format($cart['product_price'] * $cart['qty']) }}
          </li>
          @endforeach
          <li class="list-group-item d-flex justify-content-between align-items-center list-group-item-warning">
            <span>Subtotal</span>
            <span>Rp.{{ number_format($subtotal) }}</span>
          </li>
        </ul>
        <div class="form-group py-3">
          <button type="submit" class="btn btn-primary btn-sm">Bayar Sekarang</button>
        </div>
        </form>
      </div>
    </div>
  </div>


  {{-- footer --}}
  <script src="{{asset('assets/catering/js/jquery-3.2.1.min.js')}}"></script>
  <script src="{{asset('assets/catering/js/bootstrap.min.js')}}"></script>
</body>

</html>