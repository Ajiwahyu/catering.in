@extends('layouts.catering')

@section('title')
  <title>{{$product->name}}</title>
@endsection

@section('content')
    <div class="row">
      <div class="col">
        <div class="image">
          <img src="{{asset('storage/images/product/'.$product->image)}}" alt="{{$product->slug}}" class="img-fluid">
        </div>
      </div>
      <div class="col">
        <h1>{{$product->name}}</h1>
        <p>{{$product->description}}</p>
        <p>{{'Rp.'. number_format($product->price)}}</p>
        
        <!-- TAMBAHKAN FORM ACTION -->
        <form action="{{ route('front.add_cart') }}" method="POST">
          @csrf
          <div class="product_count">
            @error('qty')
                <p class="text-danger">{{$message}}</p>
            @enderror
            <label for="qty">Quantity:</label>
            <input type="text" name="qty" id="sst" min="10" value="10" title="Quantity:" class="input-text qty">
            
            <!-- BUAT INPUTAN HIDDEN YANG BERISI ID PRODUK -->
            <input type="hidden" name="product_id" value="{{ $product->id }}" class="form-control">
            
            <button onclick="var result = document.getElementById('sst'); var sst = result.value; if( !isNaN( sst )) result.value++;return false;"
            class="increase items-count" type="button">
              up
            </button>
            <button onclick="var result = document.getElementById('sst'); var sst = result.value; if( !isNaN( sst ) &amp;&amp; sst > 0 ) result.value--;return false;"
            class="reduced items-count" type="button">
              down
            </button>
          </div>
          <div class="card_area">
            
            <!-- UBAH JADI BUTTON -->
            <button class="main_btn">Add to Cart</button>
            <!-- UBAH JADI BUTTON -->
            
          </div>
        </form>

      </div>
    </div>
@endsection

