<!doctype html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
	<link rel="stylesheet" href="{{asset('assets/css/catering/register.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/standart.css')}}">
	<!-- Bootstrap CSS -->
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
		integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">

	<title>Login</title>
</head>

<body>
	<div class="container main__content d-flex align-items-center p-0">
		<div class="row d-flex bg p-5 w-100 ml-3">
			<div class="col-md d-flex justify-content-center align-items-center">
				<img src="{{asset('assets/img/undraw_healthy_options_sdo3.png')}}" alt="logo-login">
			</div>
			<div class="col-md">
				<div class="title text-center">
					<h3>Login your account now</h3>
					<p class="m-0">with login you can start ordering</p>
					<p class="m-0">safely and comfortably</p>
				</div>
				<div class="row">
					<div class="col p-5">
						@if (session('error'))
						<div class="alert alert-danger">{{session('error')}}
						</div>
						@endif
						<form action="{{route('customer.post_login')}}" method="POST">
							@csrf
							<div class="mt-3 input__in">
								<input type="email" name="email" class="form-control border-0 input__form"
									placeholder="Email" required>
							</div>
							<div class="mt-3 input__in">
								<input type="password" name="password" class="form-control border-0 input__form"
									placeholder="Password" required>
							</div>
							<div class="mb-3 text-center py-2">
								<button type="submit" class="btn"
									style="background-color: #1DB954; color: white;">Login</button>
							</div>
							<div class="login">
								<p class="p-0 m-0">Don't have an account?</p>
								<a href="{{route('customer.register')}}"
									class="fw-bold text-danger text-decoration-none">Register</a>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Option 1: Bootstrap Bundle with Popper -->
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"
		integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous">
	</script>
</body>

</html>