<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Invoice</title>

  {{-- css --}}
  <link rel="stylesheet" href="{{asset('assets/css/invoice.css')}}">
  <link rel="stylesheet" href="{{asset('assets/css/standart.css')}}">

  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
</head>

<body>

  <div class="container py-5">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Invoice</li>
      </ol>
    </nav>

    @if (session('errors'))
    <div class="alert alert-danger">
      ada kesalahan saat menginputkan, silakan coba lagi
    </div>
    @endif
    {{-- menampilkan konfirmasi pembayaran --}}
    @if ($payment != null)
    @if ($payment->status == 0)
    <div class="alert alert-warning">
      Konfirmasi pembayaran akan dicek, silakan tunggu 1 jam, jika tidak ada perubahan silakan menghubungi kami.
    </div>
    @endif
    @endif
    <div class="content p-3">
      <h1>Invoice</h1>

      <div class="row">
        <div class="col">
          {{-- <h6>Informasi Pemesan</h6> --}}
          <ul class="list-inline">
            <li>Nama Lengkap</li>
            <li>
              <h5>{{$order->customer_name}}</h5>
            </li>
            <li>No.Telephone</li>
            <li>
              <h5>{{$order->customer_phone}}</h5>
            </li>
            <li>Alamat</li>
            <li>
              <h5>{{$order->customer_address}}</h5>
            </li>
          </ul>
        </div>
        <div class="col">
          {{-- <h6>Informasi Pesanan</h6> --}}
          <ul class="list-inline">
            <li>Invoice</li>
            <li>
              <h5>{{$order->invoice}}</h5>
            </li>
            <li>Tanggal</li>
            <li>
              <h5>{{$order->created_at->format('d-m-Y')}}</h5>
            </li>
          </ul>
        </div>
        <div class="col status">
          <h6>Status</h6>
          @if ($order->status == 0)
          <h1 class="text-danger">Belum dibayar</h1>
          @else
          <h1 class="text-success">Dibayar</h1>
          @endif
        </div>
      </div>

      <table class="table my-3">
        <thead>
          <tr>
            <th>Name</th>
            <th class="text-end">Price</th>
            <th class="text-end">Qty</th>
            <th class="text-end">Subtotal</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($orderdetail as $item)
          <tr>
            <td>{{$item->product->name}}</td>
            <td class="text-end">IDR. {{number_format($item->price)}}</td>
            <td class="text-end">{{$item->qty}}</td>
            <td class="text-end">IDR. {{ number_format($item->price * $item->qty) }}</td>
          </tr>
          @endforeach
          <tr>
            <th colspan="3" class="text-end">Total</th>
            <th class="text-end">IDR. {{ number_format($total)}}</th>
          </tr>
        </tbody>
      </table>

      <div class="row py-2">
        <div class="col">
          <button type="button" data-bs-toggle="modal" data-bs-target="#confirmPayment"
            class="btn btn-primary">Konfirmasi Pembayaran</button>
        </div>
        <div class="col text-end">
          @if ($order->status != 1)
          <button type="button" data-bs-toggle="modal" data-bs-target="#paymentModal"
            class="btn btn-success w-25">Bayar</button>
          @endif
        </div>
      </div>

    </div>
    <div class="py-3">
      <a href="{{route('customer.profile')}}"><img src="{{asset('')}}assets/img/keyboard_backspace-24px.png" alt=""
          style="padding-left: 18px;"><span style="color: #F97400; padding-left: 10px; font-weight: bold;">Back to
          Profile</span></a>
    </div>

  </div>

  <!-- Modal -->
  <div class="modal fade" id="paymentModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="staticBackdropLabel">Payment</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <img src="{{asset('assets/img/codebank.jpg')}}" alt="code-bank-catering">
        </div>
      </div>
    </div>
  </div>

  {{-- modal konfirmasi pembayaran --}}
  <div class="modal fade" id="confirmPayment" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="staticBackdropLabel">Konfirmasi Pembyaran</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">

          <form action="{{route('customer.payment_proses', $order->id)}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="mb-3">
              <label class="form-label">Invoice</label>
              <input type="text" name="invoice" class="form-control" value="{{$order->invoice}}" readonly>
            </div>
            <div class="mb-3">
              <label class="form-label">Nama Rekening Pengirim</label>
              <input type="text" name="name_transfer" class="form-control input__form" value="{{old('name_transfer')}}">
              @error('name_transfer')
              <small class="text-danger">{{$message}}</small>
              @enderror
            </div>
            <div class="mb-3">
              <div class="row">
                <div class="col">
                  <label class="form-label">Jumlah</label>
                  <input type="number" name="amount" class="form-control input__form" value="{{old('amount')}}">
                  @error('amount')
                  <small class="text-danger">{{$message}}</small>
                  @enderror
                </div>
                <div class="col">
                  <label class="form-label">Tanggal Transfer</label>
                  <input type="date" name="transfer_date" class="form-control input__form"
                    value="{{old('transfer_date')}}">
                  @error('transfer_date')
                  <small class="text-danger">{{$message}}</small>
                  @enderror
                </div>
              </div>
            </div>
            <div class="mb-3">
              <label class="form-label">Nama Bank Pengirim</label>
              <input type="text" name="name_bank_transfer" class="form-control input__form"
                value="{{old('name_bank_transfer')}}">
              @error('name_bank')
              <small class="text-danger">{{$message}}</small>
              @enderror
            </div>
            <div class="mb-3">
              <label class="form-label">Upload Bukti Transfer</label>
              <input type="file" name="image_transfer" class="form-control input__form"
                value="{{old('image_transfer')}}">
              @error('image_transfer')
              <small class="text-danger">{{$message}}</small>
              @enderror
            </div>
            <div class="mb-3 text-end">
              <button type="submit" class="btn btn-primary w-25">Konfirmasi</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>




  {{-- footer --}}

  {{-- js --}}
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous">
  </script>
</body>

</html>