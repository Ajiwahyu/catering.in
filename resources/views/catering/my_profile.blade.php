<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="{{asset('assets/css/catering/profile.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/standart.css')}}">

    <link rel="stylesheet" href="{{asset('assets/plugins/fontawesome-free/css/all.css')}}">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <title>Your Profile</title>
    <script src="{{asset('assets/js/jquery.min.js')}}"></script>
</head>

<body>
    @include('layouts.catering.module.header')
    <div class="container pt-5 mt-5">
        @if (session('success'))
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="alert alert-success" role="alert">
                    {{session('success')}}
                </div>
            </div>
        </div>
        @endif
        <div class="row justify-content-center">
            <div class="col-md-2 submenu">
                <a href="{{route('customer.order')}}" class="submenu__list">My Orders</a>
                <a href="" class="submenu__active submenu__list">My Account</a>
                <a href="{{route('customer.logout')}}" class="submenu__list">Logout</a>
            </div>
            <div class="col-md-6 mainmenu">
                <a href=""><img src="img/keyboard_backspace-24px black.png" alt="" style="margin-right: 5px;">Edit
                    Account</a>
                <div class="row mt-3">
                    <div class="col-md-12">
                        <form action="{{route('customer.update_profile', $customer->id)}}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="col-md-12">
                                <label class="form-label">Full Name</label>
                                <input type="text" name="name" class="form-control input__form"
                                    value="{{($customer) ? $customer->name : ''}}">
                            </div>
                            @error('name')
                            <small class="text-danger">{{$message}}</small>
                            @enderror
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-6">
                        <div class="col-md-12">
                            <label class="form-label">Email</label>
                            <input type="email" name="email" class="form-control input__form"
                                value="{{($customer) ? $customer->email : ''}}">
                        </div>
                        @error('email')
                        <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="col-md-6">
                        <label for="phone1">Phone Number</label>
                        <div class="input-group flex-nowrap mt-2">
                            <span class="input-group-text">+62</span>
                            <input type="number" class="form-control input__form" name="phone_number"
                                value="{{($customer) ? $customer->phone_number : ''}}">
                        </div>
                        @error('phone_number')
                        <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-12">
                        <label>Address</label>
                        <div class="form-floating">
                            <textarea name="address" class="form-control mt-2 input__form p-2"
                                style="height: 100px">{{($customer) ? $customer->address : ''}}</textarea>
                        </div>
                        @error('address')
                        <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                </div>
                {{-- <div class="row mt-3">
                    <div class="col-md-6">
                        <label for="city">City</label>
                        <select class="form-select mt-2" aria-label="Default select example" id="city">
                            <option selected>PLEASE SELECT</option>
                            <option value="1">Yogyakarta</option>
                            <option value="2">Sleman</option>
                            <option value="3">Bantul</option>
                            <option value="4">Kulon Progo</option>
                            <option value="5">Gunung Kidul</option>
                        </select>
                    </div>
                    <div class="col-md-6">
                        <label for="district">District</label>
                        <select class="form-select mt-2" aria-label="Default select example" id="district">
                            <option selected>PLEASE SELECT</option>
                            <option value="1">Depok</option>
                            <option value="2">GIwangan</option>
                            <option value="3">Caturtunggal</option>
                        </select>
                    </div>
                </div> --}}
                <div class="row mt-5">
                    <div class="col-md-12">
                        <div class="d-grid gap-2">
                            <button type="submit" class="btn btn-save" type="button">Update</button>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous">
    </script>
    <script>
        $(document).ready(function(){
        $('.navbar').removeClass('change')
          
      })
    </script>
</body>

</html>