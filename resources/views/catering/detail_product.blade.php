@extends('layouts.catering2')

@section('head')
<title>Menu Detail</title>

<link rel="stylesheet" href="{{asset('assets/css/styleDetailMenu.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/standart.css')}}">
@endsection

@section('content')
<div class="container" id="container-view-home">
    <div class="row px-2">
        <div class="col-md-12">
            <div class="row menu">
                @if (session('info'))
                <div class="alert alert-success mx-3" role="alert">
                    {{session('info')}}
                </div>
                @endif
                <div class="col-md-5">
                    <h5>
                        <a href="#"> Menu</a> /
                        <a>Detail</a>
                    </h5>
                    <img src="{{asset('storage/images/product/'. $product->image)}}" class="rounded" width="500"
                        alt="" />
                    <div class="p-1 bd-highlight mt-4">
                        <button type="button" class="btn btn-outline-warning btn-sm">
                            min 10 pax
                        </button>
                        <button type="button" class="btn btn-outline-warning btn-sm">
                            min pemesanan H-1
                        </button>
                    </div>
                </div>
                <div class="col-md-5 p-4">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 class="hindai__font">{{$product->name}}</h1>
                            <p class="desc">{{Str::limit($product->description, 250)}}</p>
                            <span>IDR {{number_format($product->price)}}</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 mt-3">
                            @error('qty')
                            <small class="mb-1 text-danger d-block">Minimal Pesanan 10 Pax. </small>
                            @enderror
                            <form action="{{route('front.add_cart')}}" method="post">
                                @csrf
                                <input class="input__qty" type="number" id="quantity" name="qty" value="10" />
                                <!-- INPUTAN HIDDEN YANG BERISI ID PRODUK -->
                                <input type="hidden" name="product_id" value="{{ $product->id }}">
                                <input type="submit" class="mx-2 hindai__font" value="ADD TO CART" />
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 p-4"></div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid p-0" id="container-menu">
    <div class="container mb-5">
        <hr />
        <div class="row">
            <h1>Other Menu</h1>
            @foreach ($products as $item)
            <div class="col-md-3 my-4">
                <div class="container-fluid d-flex justify-content-center">
                    <div class="content">
                        <div class="content-overlay">
                            <img class="content-image rounded" height="200" width="300"
                                src="{{asset('storage/images/product/'.$item->image)}}" />
                        </div>
                    </div>
                </div>
                <h4 class="p-1 mt-3 mb-2 hindai__font">{{$item->name}}</h4>
                <div class="d-flex mb-3">
                    <div class="p-1 bd-highlight mr-4">
                        <button type="button" class="btn btn-outline-warning btn-sm">
                            min 10 pax
                        </button>
                    </div>
                    <div class="p-1 bd-highlight">
                        <button type="button" class="btn btn-outline-warning btn-sm">
                            min pemesanan H-1
                        </button>
                    </div>
                </div>
                <div class="d-flex bd-highlight detail">
                    <div class="p-1 me-2 bd-highlight">
                        <a class="btn-detail btn btn-success" href="{{route('front.show_product',$item->slug)}}">
                            Detail
                        </a>
                        {{-- <button type="button" class="btn btn-success">
                    Detail
                </button> --}}
                    </div>
                    <div class="p-1 flex-shrink-1 bd-highlight">
                        <h6 class="mb-2 p-1 ms-5">
                            <span>IDR {{number_format($item->price)}}</span>
                        </h6>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    $(document).ready(function(){
    $('.navbar').removeClass('change')
})
</script>
@endsection