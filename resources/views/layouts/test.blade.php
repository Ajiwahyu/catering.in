@extends('layouts.catering2')

@section('head')
    <title>Menu Detail</title>

    <link rel="stylesheet" href="{{asset('assets/css/styleDetailMenu.css')}}">
@endsection

@section('content')
<div class="container" id="container-view-home">
  <div class="row px-2">
      <div class="col-md-12">
          <div class="row menu">
              <div class="col-md-5">
                  <h5>
                    <a href="menuPage.html"> Menu</a> /
                    <a>Detail</a>
                  </h5>
                  <img
                      src="{{asset('')}}assets/img/sate.jpg"
                      class="rounded"
                      width="500"
                      alt=""
                  />
                  <div class="p-1 bd-highlight mt-4">
                      <button
                          type="button"
                          class="btn btn-outline-warning btn-sm"
                      >
                          min 10 pax
                      </button>
                      <button
                          type="button"
                          class="btn btn-outline-warning btn-sm"
                      >
                          min pemesanan H-1
                      </button>
                  </div>
              </div>
              <div class="col-md-5 p-4">
                  <div class="row">
                      <div class="col-md-12">
                          <h1>Grilled Chicken Wings</h1>
                          <p>
                              These simple Grilled Chicken Wings,
                              served with a spicy, creamy sauce.
                          </p>
                          <span>IDR 12.000</span>
                      </div>
                  </div>
                  <div class="row mt-5">
                      <div class="col-md-12 mt-5">
                          <form
                              class=""
                              action="index.html"
                              method="post"
                          >
                              <input
                                  type="number"
                                  id="quantity"
                                  name="quantity"
                                  min="10"
                              />
                              <input
                                  type="submit"
                                  value="ADD TO CART"
                              />
                          </form>
                      </div>
                  </div>
              </div>
              <div class="col-md-2 p-4"></div>
          </div>
      </div>
  </div>
</div>

<div class="container" id="container-menu">
  <hr />
  <div class="row">
      <h1>Other Menu</h1>
      <div class="col-md-3">
          <div class="container-fluid d-flex justify-content-center">
              <div class="content">
                  <a href="Pdetailsale4.html">
                      <div class="content-overlay"></div>
                      <img
                          class="content-image rounded"
                          height="200"
                          src="{{asset('')}}assets/img/sate.jpg"
                      />
                  </a>
              </div>
          </div>
          <h4 class="p-1 mt-3 mb-2">Sate Taichan</h4>
          <div class="d-flex mb-3">
              <div class="p-1 bd-highlight mr-4">
                  <button
                      type="button"
                      class="btn btn-outline-warning btn-sm"
                  >
                      min 10 pax
                  </button>
              </div>
              <div class="p-1 bd-highlight">
                  <button
                      type="button"
                      class="btn btn-outline-warning btn-sm"
                  >
                      min pemesanan H-1
                  </button>
              </div>
          </div>
          <div class="d-flex bd-highlight detail">
              <div class="p-1 me-2 bd-highlight">
                  <button type="button" class="btn btn-success">
                      Detail
                  </button>
              </div>
              <div class="p-1 flex-shrink-1 bd-highlight">
                  <h6 class="mb-2 p-1 ms-5">
                      <span>IDR 85.000</span>
                  </h6>
              </div>
          </div>
      </div>
      <div class="col-md-3">
          <div class="container-fluid d-flex justify-content-center">
              <div class="content">
                  <a href="Pdetailsale4.html">
                      <div class="content-overlay"></div>
                      <img
                          class="content-image rounded"
                          height="200"
                          src="{{asset('')}}assets/img/sate.jpg"
                      />
                  </a>
              </div>
          </div>
          <h4 class="p-1 mt-3 mb-2">Sate Taichan</h4>
          <div class="d-flex mb-3">
              <div class="p-1 bd-highlight mr-4">
                  <button
                      type="button"
                      class="btn btn-outline-warning btn-sm"
                  >
                      min 10 pax
                  </button>
              </div>
              <div class="p-1 bd-highlight">
                  <button
                      type="button"
                      class="btn btn-outline-warning btn-sm"
                  >
                      min pemesanan H-1
                  </button>
              </div>
          </div>
          <div class="d-flex bd-highlight detail">
              <div class="p-1 me-2 bd-highlight">
                  <button type="button" class="btn btn-success">
                      Detail
                  </button>
              </div>
              <div class="p-1 flex-shrink-1 bd-highlight">
                  <h6 class="mb-2 p-1 ms-5">
                      <span>IDR 85.000</span>
                  </h6>
              </div>
          </div>
      </div>

      <div class="col-md-3">
          <div class="container-fluid d-flex justify-content-center">
              <div class="content">
                  <a href="Pdetailsale4.html">
                      <div class="content-overlay"></div>
                      <img
                          class="content-image rounded"
                          height="200"
                          src="{{asset('')}}assets/img/sate.jpg"
                      />
                  </a>
              </div>
          </div>
          <h4 class="p-1 mt-3 mb-2">Sate Taichan</h4>
          <div class="d-flex mb-3">
              <div class="p-1 bd-highlight mr-4">
                  <button
                      type="button"
                      class="btn btn-outline-warning btn-sm"
                  >
                      min 10 pax
                  </button>
              </div>
              <div class="p-1 bd-highlight">
                  <button
                      type="button"
                      class="btn btn-outline-warning btn-sm"
                  >
                      min pemesanan H-1
                  </button>
              </div>
          </div>
          <div class="d-flex bd-highlight detail">
              <div class="p-1 me-2 bd-highlight">
                  <button type="button" class="btn btn-success">
                      Detail
                  </button>
              </div>
              <div class="p-1 flex-shrink-1 bd-highlight">
                  <h6 class="mb-2 p-1 ms-5">
                      <span>IDR 85.000</span>
                  </h6>
              </div>
          </div>
      </div>
      <div class="col-md-3">
          <div class="container-fluid d-flex justify-content-center">
              <div class="content">
                  <a href="Pdetailsale4.html">
                      <div class="content-overlay"></div>
                      <img
                          class="content-image rounded"
                          height="200"
                          src="{{asset('')}}assets/img/sate.jpg"
                      />
                  </a>
              </div>
          </div>
          <h4 class="p-1 mt-3 mb-2">Sate Taichan</h4>
          <div class="d-flex mb-3">
              <div class="p-1 bd-highlight mr-4">
                  <button
                      type="button"
                      class="btn btn-outline-warning btn-sm"
                  >
                      min 10 pax
                  </button>
              </div>
              <div class="p-1 bd-highlight">
                  <button
                      type="button"
                      class="btn btn-outline-warning btn-sm"
                  >
                      min pemesanan H-1
                  </button>
              </div>
          </div>
          <div class="d-flex bd-highlight detail">
              <div class="p-1 me-2 bd-highlight">
                  <button type="button" class="btn btn-success">
                      Detail
                  </button>
              </div>
              <div class="p-1 flex-shrink-1 bd-highlight">
                  <h6 class="mb-2 p-1 ms-5">
                      <span>IDR 85.000</span>
                  </h6>
              </div>
          </div>
      </div>
  </div>
  <div class="row mt-5">
      <div class="col-md-3">
          <div class="container-fluid d-flex justify-content-center">
              <div class="content">
                  <a href="Pdetailsale4.html">
                      <div class="content-overlay"></div>
                      <img
                          class="content-image rounded"
                          height="200"
                          src="{{asset('')}}assets/img/sate.jpg"
                      />
                  </a>
              </div>
          </div>
          <h4 class="p-1 mt-3 mb-2">Sate Taichan</h4>
          <div class="d-flex mb-3">
              <div class="p-1 bd-highlight mr-4">
                  <button
                      type="button"
                      class="btn btn-outline-warning btn-sm"
                  >
                      min 10 pax
                  </button>
              </div>
              <div class="p-1 bd-highlight">
                  <button
                      type="button"
                      class="btn btn-outline-warning btn-sm"
                  >
                      min pemesanan H-1
                  </button>
              </div>
          </div>
          <div class="d-flex bd-highlight detail">
              <div class="p-1 me-2 bd-highlight">
                  <button type="button" class="btn btn-success">
                      Detail
                  </button>
              </div>
              <div class="p-1 flex-shrink-1 bd-highlight">
                  <h6 class="mb-2 p-1 ms-5">
                      <span>IDR 85.000</span>
                  </h6>
              </div>
          </div>
      </div>
      <div class="col-md-3">
          <div class="container-fluid d-flex justify-content-center">
              <div class="content">
                  <a href="Pdetailsale4.html">
                      <div class="content-overlay"></div>
                      <img
                          class="content-image rounded"
                          height="200"
                          src="{{asset('')}}assets/img/sate.jpg"
                      />
                  </a>
              </div>
          </div>
          <h4 class="p-1 mt-3 mb-2">Sate Taichan</h4>
          <div class="d-flex mb-3">
              <div class="p-1 bd-highlight mr-4">
                  <button
                      type="button"
                      class="btn btn-outline-warning btn-sm"
                  >
                      min 10 pax
                  </button>
              </div>
              <div class="p-1 bd-highlight">
                  <button
                      type="button"
                      class="btn btn-outline-warning btn-sm"
                  >
                      min pemesanan H-1
                  </button>
              </div>
          </div>
          <div class="d-flex bd-highlight detail">
              <div class="p-1 me-2 bd-highlight">
                  <button type="button" class="btn btn-success">
                      Detail
                  </button>
              </div>
              <div class="p-1 flex-shrink-1 bd-highlight">
                  <h6 class="mb-2 p-1 ms-5">
                      <span>IDR 85.000</span>
                  </h6>
              </div>
          </div>
      </div>
      <div class="col-md-3">
          <div class="container-fluid d-flex justify-content-center">
              <div class="content">
                  <a href="Pdetailsale4.html">
                      <div class="content-overlay"></div>
                      <img
                          class="content-image rounded"
                          height="200"
                          src="{{asset('')}}assets/img/sate.jpg"
                      />
                  </a>
              </div>
          </div>
          <h4 class="p-1 mt-3 mb-2">Sate Taichan</h4>
          <div class="d-flex mb-3">
              <div class="p-1 bd-highlight mr-4">
                  <button
                      type="button"
                      class="btn btn-outline-warning btn-sm"
                  >
                      min 10 pax
                  </button>
              </div>
              <div class="p-1 bd-highlight">
                  <button
                      type="button"
                      class="btn btn-outline-warning btn-sm"
                  >
                      min pemesanan H-1
                  </button>
              </div>
          </div>
          <div class="d-flex bd-highlight detail">
              <div class="p-1 me-2 bd-highlight">
                  <button type="button" class="btn btn-success">
                      Detail
                  </button>
              </div>
              <div class="p-1 flex-shrink-1 bd-highlight">
                  <h6 class="mb-2 p-1 ms-5">
                      <span>IDR 85.000</span>
                  </h6>
              </div>
          </div>
      </div>
      <div class="col-md-3">
          <div class="container-fluid d-flex justify-content-center">
              <div class="content">
                  <a href="Pdetailsale4.html">
                      <div class="content-overlay"></div>
                      <img
                          class="content-image rounded"
                          height="200"
                          src="{{asset('')}}assets/img/sate.jpg"
                      />
                  </a>
              </div>
          </div>
          <h4 class="p-1 mt-3 mb-2">Sate Taichan</h4>
          <div class="d-flex mb-3">
              <div class="p-1 bd-highlight mr-4">
                  <button
                      type="button"
                      class="btn btn-outline-warning btn-sm"
                  >
                      min 10 pax
                  </button>
              </div>
              <div class="p-1 bd-highlight">
                  <button
                      type="button"
                      class="btn btn-outline-warning btn-sm"
                  >
                      min pemesanan H-1
                  </button>
              </div>
          </div>
          <div class="d-flex bd-highlight detail">
              <div class="p-1 me-2 bd-highlight">
                  <button type="button" class="btn btn-success">
                      Detail
                  </button>
              </div>
              <div class="p-1 flex-shrink-1 bd-highlight">
                  <h6 class="mb-2 p-1 ms-5">
                      <span>IDR 85.000</span>
                  </h6>
              </div>
          </div>
      </div>
  </div>
</div>
@endsection