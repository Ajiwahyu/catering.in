<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  
  @yield('title')

  <link rel="stylesheet" href="{{asset('assets/catering/css/bootstrap.css')}}">

</head>
<body>

  <div class="container-fluid">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <div class="container">
        <a class="navbar-brand" href="#">Navbar</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item active">
              <a class="nav-link" href="{{route('front.index')}}">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{route('front.product')}}">Product</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{route('front.list_cart')}}">Cart</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{route('customer.register')}}">Register</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{route('customer.login')}}">Login</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{route('customer.logout')}}">Log out</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    
    <div class="container py-5">
      @yield('content')
    </div>
    
    
  <footer class="py-5 bg-light">
    <div class="container">
      <div class="row">
        <div class="col-12 col-md">
              <small class="d-block mb-3 text-muted">&copy; 2017-2019</small>
            </div>
            <div class="col-6 col-md">
              <h5>Features</h5>
              <ul class="list-unstyled text-small">
                <li><a class="text-muted" href="#">Cool stuff</a></li>
                <li><a class="text-muted" href="#">Random feature</a></li>
                <li><a class="text-muted" href="#">Team feature</a></li>
                <li><a class="text-muted" href="#">Stuff for developers</a></li>
                <li><a class="text-muted" href="#">Another one</a></li>
                <li><a class="text-muted" href="#">Last time</a></li>
              </ul>
            </div>
            <div class="col-6 col-md">
              <h5>Resources</h5>
              <ul class="list-unstyled text-small">
                <li><a class="text-muted" href="#">Resource</a></li>
                <li><a class="text-muted" href="#">Resource name</a></li>
                <li><a class="text-muted" href="#">Another resource</a></li>
                <li><a class="text-muted" href="#">Final resource</a></li>
              </ul>
            </div>
            <div class="col-6 col-md">
              <h5>Resources</h5>
              <ul class="list-unstyled text-small">
                <li><a class="text-muted" href="#">Business</a></li>
                <li><a class="text-muted" href="#">Education</a></li>
                <li><a class="text-muted" href="#">Government</a></li>
                <li><a class="text-muted" href="#">Gaming</a></li>
              </ul>
            </div>
            <div class="col-6 col-md">
              <h5>About</h5>
              <ul class="list-unstyled text-small">
                <li><a class="text-muted" href="#">Team</a></li>
                <li><a class="text-muted" href="#">Locations</a></li>
                <li><a class="text-muted" href="#">Privacy</a></li>
                <li><a class="text-muted" href="#">Terms</a></li>
              </ul>
          </div>
        </div>
      </div>
    </footer>
  </div>
  
  {{-- footer --}}
  <script src="{{asset('assets/catering/js/jquery-3.2.1.min.js')}}"></script>
  <script src="{{asset('assets/catering/js/bootstrap.min.js')}}"></script>
  @yield('js')
</body>
</html>