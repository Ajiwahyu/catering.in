<div class="container-fluid" id="container-footer">
  <div class="container">
    <div class="row mt-5">
      <div class="col-md-3 p-0">
        <div class="card-body">
          <h4 class="card-title mb-3">Catering.in</h4>
          <p class="card-text">We are a startup engaged in the catering sector by prioritizing quality and ease of
            ordering</p>
        </div>
      </div>
      <div class="col-md-2 p-0">
        <div class="card-body">
          <h4 class="card-title mb-3">Quick link</h4>
          <div class="link">
            <p> <a href="">Food Colection</a> </p>
            <p> <a href="">Services</a> </p>
            <p> <a href="">Online Order</a> </p>
            <p> <a href="">Contact</a> </p>
          </div>
        </div>
      </div>
      <div class="col-md-2 p-0">
        <div class="card-body">
          <h4 class="card-title mb-3">Services</h4>
          <div class="link">
            <p> <a href="">About</a> </p>
            <p> <a href="">Support</a> </p>
            <p> <a href="">Privacy & Policy</a> </p>
          </div>
        </div>
      </div>
      <div class="col-md-3 p-0">
        <div class="card-body">
          <h4 class="card-title mb-3">Contact Us</h4>
          <div class="row">
            <p> <a href=""><i class="fas fa-phone-alt p-1 "></i></a> 085385008809 </p>
            <p> <a href=""><i class="fas fa-envelope p-1"></i></a> Contact.cateringin@gmail.com </p>
            <p> <a href=""><i class="fas fa-map-marker-alt p-1"></i></a> Yogyakarta </p>
          </div>
        </div>
      </div>
      <div class="col-md-2 p-0">
        <div class="card-body">
          <h4 class="card-title mb-3">Follow Us</h4>
          <div class="d-flex flex-row">
            <div class="p-2"><a href=""><i class="fab fa-facebook fa-2x"></i></a></div>
            <div class="p-2"><a href=""><i class="fab fa-instagram fa-2x"></i></a></div>
            <div class="p-2"><a href=""><i class="fab fa-twitter fa-2x"></i></a></div>
            <div class="p-2"><a href=""><i class="fab fa-tiktok fa-2x"></i></a></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid" id="container-Copyright">
    <div class="row">
      <div class="col-md-12 d-flex justify-content-center">
        <h5>Copyright 2020 | Catering.in</h5>
      </div>
    </div>
  </div>
</div>