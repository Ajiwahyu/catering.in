<header>
  <nav class="navbar navbar-expand-md fixed-top justify-content-center change">
    <div class="container">
      {{-- <img src="{{asset('assets/img/logo-catering.png')}}" alt="logo-catering"> --}}
      <a href="/" class="navbar-brand d-flex w-50 mr-auto">Catering.in</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsingNavbar3">
        <span class="navbar-toggler-icon"></span>
      </button>

      <x-navbar :active="$active" />

    </div>

    </div>
  </nav>
</header>