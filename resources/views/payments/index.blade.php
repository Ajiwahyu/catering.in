@extends('layouts.admin')

@section('title')
<title>Admin | Payment</title>
@endsection

@section('content')
<main class="main">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Dashboard</li>
        <li class="breadcrumb-item active">{{$active}}</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header bg-dark">
                            <h4 class="card-title">List Payment</h4>
                        </div>
                        <div class="card-body p-0">
                            <table class="table table-dark table-striped">
                                <thead>
                                    <tr>
                                        <th scope="col">Invoice</th>
                                        <th scope="col">Name</th>
                                        <th scope="col" class="text-center">Transfer Date</th>
                                        <th scope="col" class="text-center">Amount</th>
                                        <th scope="col" class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($payments as $payment)
                                    <tr>
                                        <td>{{$payment->order->invoice}}</td>
                                        <td>{{$payment->name_transfer}} <i
                                                class="{{($payment->status == 1) ? 'fas fa-check-circle text-success' : 'fas fa-times-circle text-danger'}}"></i>
                                        </td>
                                        <td class="text-center">{{$payment->transfer_date}}</td>
                                        <td class="text-center">IDR. {{number_format($payment->amount)}}</td>
                                        <td class="text-center">
                                            <a href="{{route('payment.edit', $payment->id)}}"
                                                class="btn btn-warning btn-sm">Lihat</a>
                                        </td>
                                    </tr>
                                    @empty
                                    <tr>
                                        <td colspan="5">Tidak Ada Pembayaran</td>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection