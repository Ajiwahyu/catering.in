@extends('layouts.admin')

@section('title')
<title>Admin | Payment</title>
@endsection

@section('content')
<main class="main">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Dashboard</li>
        <li class="breadcrumb-item active">{{$active}}</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            @if (session('success'))
            <div class="success alert alert-success">
                {{session('success')}}
            </div>
            @endif
            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header bg-dark">
                            <h4 class="card-title">Payment Confirm</h4>
                        </div>
                        <div class="card-body p-3">
                            <form action="{{route('customer.payment_confirm')}}" method="POST">
                                @csrf
                                <input type="hidden" name="payment_id" value="{{$payment->id}}">
                                <div class="mb-3">
                                    <label class="form-label">Invoice</label>
                                    <input type="text" class="form-control" value="{{$payment->order->invoice}}"
                                        readonly>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Name Transfer</label>
                                    <input type="text" class="form-control" value="{{$payment->name_transfer}}"
                                        readonly>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Name Bank Transfer</label>
                                    <input type="text" class="form-control" value="{{$payment->name_bank_transfer}}"
                                        readonly>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Transfer Date</label>
                                    <input type="text" class="form-control" value="{{$payment->transfer_date}}"
                                        readonly>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Jumlah</label>
                                    <input type="text" class="form-control" value="IDR. {{$payment->amount}}" readonly>
                                </div>
                                <div class="mb">
                                    <label class="form-label">Proof</label>
                                </div>
                                <div class="images my-3 text-center">
                                    <img src="{{asset('storage/images/payment/'.$payment->image_transfer)}}" alt=""
                                        width="400">
                                </div>
                                <div class="mb-3">
                                    @if ($payment->status == 0)
                                    <button class="btn btn-primary w-100">Konfirmasi Pembayaran</button>
                                    @else
                                    <input type="button" class="btn btn-secondary w-100" value="Konfirmasi Pembayaran"
                                        readonly>
                                    @endif
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                {{-- preview barang --}}
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header bg-dark">
                            <h4 class="card-title">Preview Order</h4>
                        </div>
                        <div class="card-body">
                            <div class="">
                                <ul class="list-inline">
                                    <li class="text-bold"><small
                                            class="text-black-50">Invoice</small><br>{{$payment->order->invoice}}
                                    </li>
                                    <li class="text-bold"><small class="text-black-50">Nama
                                            Pemesan</small><br>{{$payment->order->customer_name}}
                                    </li>
                                    <li class="text-bold"><small
                                            class="text-black-50">No.Hp</small><br>{{$payment->order->customer_phone}}
                                    </li>
                                    <li class="text-bold"><small
                                            class="text-black-50">Alamat</small><br>{{$payment->order->customer_address}}
                                    </li>
                                </ul>
                                <hr>
                                <h5>Pesanan</h5>
                                @foreach ($payment->order->details as $item)
                                <div class="d-flex justify-content-between">
                                    <p class="my-1">&#8226;
                                        {{Str::limit($item->product->name, 20)}}
                                        <span class="badge bg-warning">{{$item->qty}}</span>
                                    </p>
                                    <p class="my-1 text-bold">IDR.{{number_format($item->price)}}</p>
                                </div>
                                @endforeach
                                <hr>
                                <div class="d-flex justify-content-end">
                                    <span class="text-bold">IDR. {{number_format($total)}}</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-body {{($payment->status == 1) ? 'bg-success' : 'bg-danger' }} text-center">
                            <h5 class="fw-bold">{{($payment->status == 1) ? 'Di Bayar' : 'Belum Di Bayar'}}</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection