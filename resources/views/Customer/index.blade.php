@extends('layouts.admin')

@section('title')
<title>Admin | Customer</title>
@endsection

@section('content')
<main class="main">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Dashboard</li>
        <li class="breadcrumb-item active">{{$active}}</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    @if (session('success'))
                    <div class="alert alert-success">
                        {{session('success')}}
                    </div>
                    @endif
                    <div class="card">
                        <div class="card-header bg-dark">
                            <h4 class="card-title">Customer</h4>
                        </div>
                        <div class="card-body p-0">
                            <table class="table table-hover table-dark">
                                <thead>
                                    <tr>
                                        <th scope="col">No.</th>
                                        <th scope="col">Name</th>
                                        <th scope="col">Email</th>
                                        <th scope="col">Status</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($customers as $customer)
                                    <tr>
                                        <th scope="row">{{$loop->iteration}}.</th>
                                        <td>{{$customer->name}}</td>
                                        <td>{{$customer->email}}</td>
                                        <td>
                                            @if ($customer->status == 0)
                                            <span class="badge badge-secondary">Tidak Aktif</span>
                                            @else
                                            <span class="badge badge-success">Aktif</span>
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{route('customer.edit', $customer->id)}}"
                                                class="btn btn-sm btn-warning">Edit</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection