@extends('layouts.admin')

@section('title')
<title>Admin | Edit Customer</title>
@endsection

@section('content')
<main class="main">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Dashboard</li>
        <li class="breadcrumb-item active">{{$active}}</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card mx-3">
                        <div class="card-header">
                            <h4 class="card-title">Customer</h4>
                        </div>
                        <div class="card-body">
                            <form action="{{route('customer.update', $customer->id)}}" method="POST">
                                @csrf
                                @method('PUT')
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Status</label>
                                    <div class="col-sm-10">
                                        <select class="form-control" name="status">
                                            <option value="1" {{($customer->status === 1) ? 'selected' : ''}}>Aktif
                                            </option>
                                            <option value="0" {{($customer->status === 0) ? 'selected' : ''}}>Tidak
                                                Aktif
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Full Name</label>
                                    <div class="col-sm-10">
                                        <input type="text" value="{{$customer->name}}" class="form-control input__form"
                                            readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Email Address</label>
                                    <div class="col-sm-10">
                                        <input type="email" value="{{$customer->email}}" class="form-control" readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Phone Number</label>
                                    <div class="col-sm-10 input-group">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">+62</div>
                                        </div>
                                        <input type="number" class="form-control" value="{{$customer->phone_number}}"
                                            readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Address</label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control" rows="8"
                                            readonly>{{$customer->address}}</textarea>
                                    </div>
                                </div>
                                <div class="row justify-content-end px-2">
                                    <button type="submit" class="btn btn-primary w-25">Update</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection