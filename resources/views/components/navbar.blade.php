<div class="navbar-collapse collapse w-100">
  <ul class="nav navbar-nav ml-auto w-100 justify-content-end">
    @foreach ($lists as $list)
    <li class="nav-item">
      <a class="nav-link {{($isActive($list['label'])) ? 'active' : ''}}"
        href="{{route($list['link'])}}">{{$list['label']}}
      </a>
    </li>
    @endforeach
    <li class="nav-item">
      <a class="nav-link invisible " href="#">Contact</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{route('front.list_cart')}}"><i class="fa fa-shopping-cart fa-lg mx-2"
          aria-hidden="true"></i>Cart
        @if ($cookie)
        <b class="text-dark">{{count($cookie)}}</b>
        @endif
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="/profile"><i class="fa fa-user-circle fa-lg mx-2" aria-hidden="true"></i>Profile</a>
    </li>
  </ul>

</div>