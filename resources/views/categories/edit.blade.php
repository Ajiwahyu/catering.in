@extends('layouts.admin')

@section('title')
    <title>Admin | Edit Category</title>
@endsection

@section('content')
<main class="main">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Dashboard</li>
        <li class="breadcrumb-item active">{{$active}}</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">     
                
              <div class="col px-4">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Edit Category</h4>
                    </div>
                    <div class="card-body">
                        
                        <form action="{{route('category.update', $category->id)}}" method="POST">
                          @csrf
                          @method('PUT')
                            <div class="form-group">
                              <label for="name">Nama Kategori</label>
                              <input type="text" name="name" class="form-control" value="{{ $category->name}}">
                              @error('name')
                                  <small class="text-danger">{{$message}}</small>
                              @enderror
                            </div>
                            <div class="form-group">
                              <label for="inputState">Status</label>
                              <select id="inputState" name="status" class="form-control">
                                <option {{($category->status === 1) ? 'selected' : null}} value='1'>Aktif</option>
                                <option {{($category->status === 0) ? 'selected' : null}} value='0'>Tidak Aktif</option>
                              </select>
                            </div>
                            <div class="form-group">
                              <button type="submit" class="btn btn-primary float-right">Update Category</button>
                            </div>
                        </form>
                        
                    </div>
                </div>
              </div>

            </div>
        </div>
    </div>
</main>
@endsection