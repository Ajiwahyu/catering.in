<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', [App\Http\Controllers\Catering\FrontController::class, 'index'])->name('front.index');
Route::get('/product', [App\Http\Controllers\Catering\FrontController::class, 'product'])->name('front.product');
Route::get('/faq', [App\Http\Controllers\Catering\FrontController::class, 'getFaq'])->name('front.faq');
Route::get('/contact', [App\Http\Controllers\Catering\FrontController::class, 'getContact'])->name('front.contact');
Route::post('/sendemail', [App\Http\Controllers\Catering\FrontController::class, 'sendEmail'])->name('front.post_contact');
// Route::get('/product/{slug}', [App\Http\Controllers\Catering\FrontController::class, 'show'])->name('front.show_product');
// Route::get('/profile', [App\Http\Controllers\Catering\ProfileController::class, 'index'])->name('customer.profile');



Route::group(['prefix' => 'member', 'namespace' => 'catering'], function() {
    Route::get('/register', [App\Http\Controllers\Catering\RegisterController::class, 'registerForm'])->name('customer.register');
    Route::post('/register', [App\Http\Controllers\Catering\RegisterController::class, 'register'])->name('customer.post_register');
    Route::get('/login', [App\Http\Controllers\Catering\LoginController::class, 'loginForm'])->name('customer.login');
    Route::post('/login', [App\Http\Controllers\Catering\LoginController::class, 'login'])->name('customer.post_login');
    Route::get('/verify/{token}', [App\Http\Controllers\Catering\FrontController::class, 'verifyCustomerRegistration'])->name('customer.verify');
});

Route::group(['middleware' => 'customer'], function() {
    // product
    Route::get('/product/{slug}', [App\Http\Controllers\Catering\FrontController::class, 'show'])->name('front.show_product');
    // cart
    Route::get('/cart', [App\Http\Controllers\Catering\CartController::class, 'listCart'])->name('front.list_cart');
    Route::post('/cart', [App\Http\Controllers\Catering\CartController::class, 'addToCart'])->name('front.add_cart');
    Route::post('/cart/update', [App\Http\Controllers\Catering\CartController::class, 'updateCart'])->name('front.update_cart');
    Route::get('/cart/{id}/delete', [App\Http\Controllers\Catering\CartController::class, 'deleteCart'])->name('front.delete_cart');
    // checkout
    Route::get('/checkout', [App\Http\Controllers\Catering\CartController::class, 'checkout'])->name('front.checkout');
    Route::get('/checkout/{invoice}', [App\Http\Controllers\Catering\CartController::class, 'checkoutFinish'])->name('front.finish_checkout');
    Route::post('/checkout', [App\Http\Controllers\Catering\CartController::class, 'processCheckout'])->name('front.store_checkout');
    // profile
    Route::get('/dashboard', [App\Http\Controllers\Catering\LoginController::class, 'dashboard'])->name('customer.dashboard');
    Route::get('/logout', [App\Http\Controllers\Catering\LoginController::class, 'logout'])->name('customer.logout');
    Route::get('/profile', [App\Http\Controllers\Catering\ProfileController::class, 'myProfile'])->name('customer.profile');
    Route::put('/profile/{id}', [App\Http\Controllers\Catering\ProfileController::class, 'updateMyProfile'])->name('customer.update_profile');
    Route::get('/order', [App\Http\Controllers\Catering\ProfileController::class, 'myOrder'])->name('customer.order');
    // payment
    Route::post('/paymentproses/{id_order}', [App\Http\Controllers\Catering\PaymentControlelr::class, 'paymentProses'])->name('customer.payment_proses');
    Route::post('/paymentconfirm', [App\Http\Controllers\Catering\PaymentControlelr::class, 'paymentConfirm'])->name('customer.payment_confirm');
});

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['prefix' => 'administrator', 'middleware' => 'auth'], function(){
    // Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    // Route::resource();
    Route::resource('category', 'App\Http\Controllers\CategoryController');
    Route::resource('order', 'App\Http\Controllers\OrderController');
    Route::resource('customer', 'App\Http\Controllers\CustomerController');
    Route::resource('product', 'App\Http\Controllers\ProductController');
    Route::resource('payment', 'App\Http\Controllers\PaymentController');
    Route::get('/signout', [App\Http\Controllers\HomeController::class, 'signout'])->name('signout');
});

Route::get('/tes', function(){
    $active = 'p';
    return view('catering.my_order', compact('active'));
});